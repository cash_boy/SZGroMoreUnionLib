package com.cn.shuangzi.ad.custom;

import com.bytedance.sdk.openadsdk.mediation.bridge.custom.MediationCustomInitLoader;

/**
 * Created by CN.
 */
public abstract class SZMediationCustomInitLoader extends MediationCustomInitLoader {
    public boolean isInitSuccess = false;
    public boolean isBeginInit = false;
}
