package com.cn.shuangzi.ad.custom;

import android.Manifest;
import android.content.Context;

import com.bytedance.sdk.openadsdk.mediation.bridge.custom.MediationCustomInitLoader;
import com.bytedance.sdk.openadsdk.mediation.custom.MediationCustomInitConfig;
import com.cn.shuangzi.ad.ADManager;
import com.cn.shuangzi.ad.util.ADUtil;
import com.cn.shuangzi.ad.util.ThreadUtils;
import com.cn.shuangzi.util.SZUtil;
import com.qq.e.comm.managers.GDTAdSdk;
import com.qq.e.comm.managers.setting.GlobalSetting;
import com.qq.e.comm.managers.status.SDKStatus;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * YLH 自定义初始化类
 */
public class YlhCustomerConfig extends SZMediationCustomInitLoader {
    @Override
    public void initializeADN(final Context context, final MediationCustomInitConfig mediationCustomInitConfig, Map<String, Object> map) {
        SZUtil.logError("优量汇sdk判断是否初始化："+isInit());
        SZUtil.logError("初始化成功："+isInitSuccess);
        SZUtil.logError("开始初始化："+isBeginInit);
        if(!isInit()&&!isInitSuccess&&!isBeginInit) {
            isBeginInit = true;
            /**
             * 在子线程中进行初始化
             */
            ThreadUtils.runOnThreadPool(new Runnable() {
                @Override
                public void run() {
                    SZUtil.logError("开始初始化优量汇，当前进程："+ADUtil.getCurrentProcessName(context));
                    SZUtil.logError("当前YlhCustomerConfig："+YlhCustomerConfig.this);
                    GlobalSetting.setChannel(ADManager.getGDTChannelCode());
                    boolean isCanUsePhone = ADUtil.isCanUsePermission(Manifest.permission
                            .READ_PHONE_STATE);
                    GlobalSetting.setAgreeReadDeviceId(isCanUsePhone);
                    GlobalSetting.setAgreeReadAndroidId(isCanUsePhone);
                    GlobalSetting.setAgreePrivacyStrategy(isCanUsePhone);
                    GlobalSetting.setEnableCollectAppInstallStatus(isCanUsePhone);
                    GlobalSetting.setPersonalizedState(isCanUsePhone ? 0 : 1);//优量汇个性化推荐广告开关，0为开启个性化推荐广告，1为屏蔽个性化推荐广告
                    GDTAdSdk.initWithoutStart(context, mediationCustomInitConfig.getAppId()); // 调用此接口进行初始化，该接口不会采集用户信息
                    // 调用initWithoutStart后请尽快调用start，否则可能影响广告填充，造成收入下降
                    GDTAdSdk.start(new GDTAdSdk.OnStartListener() {
                        @Override
                        public void onStartSuccess() {
                            isBeginInit =false;
                            isInitSuccess = true;
                            SZUtil.logError("优量汇初始化成功");
                            // 推荐开发者在onStartSuccess回调后开始拉广告
                            //初始化成功回调
                            callInitSuccess();
                        }

                        @Override
                        public void onStartFailed(Exception e) {
                            isBeginInit =false;
                            isInitSuccess = false;
                            SZUtil.logError("优量汇初始化失败："+e.toString());
                        }
                    });
                }
            });
        }
    }

    @Override
    public String getNetworkSdkVersion() {
        return SDKStatus.getIntegrationSDKVersion();
    }

    @Override
    public String getBiddingToken(Context context, Map<String, Object> extra) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> future = executor.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return GDTAdSdk.getGDTAdManger().getBuyerId(null); //开发者可不传;
            }
        });
        try {
            return future.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String getSdkInfo(Context context, final Map<String, Object> extra) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<String> future = executor.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                String posId = (String) extra.get("slot_id");
                return GDTAdSdk.getGDTAdManger().getSDKInfo(posId);
            }
        });
        try {
            return future.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }

}
