package com.cn.shuangzi.ad.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.cn.shuangzi.SZManager;
import com.cn.shuangzi.ad.R;
import com.cn.shuangzi.common.SZConst;

import androidx.annotation.NonNull;

/**
 * Created by CN.
 */

public class AlertRemoveAdDialog extends Dialog {
    private OnClickRemoveAdListener onClickRemoveAdListener;

    public AlertRemoveAdDialog(@NonNull Context context, OnClickRemoveAdListener onClickRemoveAdListener) {
        super(context, R.style.alert_dialog);
        this.onClickRemoveAdListener = onClickRemoveAdListener;
        setCancelable(true);
        setCanceledOnTouchOutside(false);
        setContentView(R.layout.alert_no_ad);
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        findViewById(R.id.imgClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        findViewById(R.id.txtNoAdVip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
                if (onClickRemoveAdListener != null) {
                    onClickRemoveAdListener.onClickBuyVip();
                    try {
                        SZManager.getInstance().onUMEvent(SZConst.EVENT_VIP_CLICK_CLOSE_INTERACTION_AD);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            }
        });
        findViewById(R.id.txtNoAdRewardVideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
                if (onClickRemoveAdListener != null) {
                    onClickRemoveAdListener.onClickRewardVideo();
                    try {
                        SZManager.getInstance().onUMEvent(SZConst.EVENT_VIDEO_TASK_CLICK_CLOSE_INTERACTION_AD);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
            }
        });
    }

    public interface OnClickRemoveAdListener {
        void onClickBuyVip();

        void onClickRewardVideo();
    }
}
