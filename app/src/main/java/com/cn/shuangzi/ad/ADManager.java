package com.cn.shuangzi.ad;

import android.Manifest;
import android.content.Context;
import android.text.TextUtils;

import com.cn.shuangzi.SZApp;
import com.cn.shuangzi.SZManager;
import com.cn.shuangzi.ad.bean.ADPlatform;
import com.cn.shuangzi.ad.bean.AdShowTypeInfo;
import com.cn.shuangzi.ad.bean.KsErrorInfo;
import com.cn.shuangzi.ad.bean.YlhErrorInfo;
import com.cn.shuangzi.ad.retrofit.ADHttpRequestRetrofitService;
import com.cn.shuangzi.ad.retrofit.ADRequestInterceptor;
import com.cn.shuangzi.ad.util.ADConst;
import com.cn.shuangzi.ad.util.ADUtil;
import com.cn.shuangzi.ad.util.tt.TTAdManagerHolder;
import com.cn.shuangzi.bean.MaterialsInfo;
import com.cn.shuangzi.retrofit.SZRetrofitManager;
import com.cn.shuangzi.retrofit.SZRetrofitResponseListener;
import com.cn.shuangzi.util.SZDateUtil;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.cn.shuangzi.util.SZXmlUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kwad.sdk.api.KsInterstitialAd;
import com.kwad.sdk.api.KsRewardVideoAd;
import com.kwad.sdk.api.KsSplashScreenAd;
import com.kwad.sdk.api.model.AdnName;
import com.qq.e.ads.banner2.UnifiedBannerView;
import com.qq.e.ads.interstitial2.UnifiedInterstitialAD;
import com.qq.e.ads.rewardvideo.RewardVideoAD;
import com.qq.e.ads.splash.SplashAD;
import com.qq.e.comm.managers.GDTAdSdk;
import com.qq.e.comm.managers.setting.GlobalSetting;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.cn.shuangzi.ad.util.ADConst.AD_LOAD_BANNER_POLICY;
import static com.cn.shuangzi.ad.util.ADConst.AD_LOAD_INTERACTION_POLICY;
import static com.cn.shuangzi.ad.util.ADConst.AD_LOAD_NATIVE_POLICY;
import static com.cn.shuangzi.ad.util.ADConst.AD_LOAD_POLICY;
import static com.cn.shuangzi.ad.util.ADConst.AD_LOAD_SELF_BANNER_POLICY;
import static com.cn.shuangzi.ad.util.ADConst.AD_LOAD_SELF_FLOAT_POLICY;
import static com.cn.shuangzi.ad.util.ADConst.AD_LOAD_SPLASH_POLICY;
import static com.cn.shuangzi.ad.util.ADConst.AD_LOCAL_ERROR_EVENT_ID;
import static com.cn.shuangzi.ad.util.ADConst.AD_PLATFORM_LOCAL;
import static com.cn.shuangzi.ad.util.ADConst.AD_READ_LOCAL_EVENT_ID;
import static com.cn.shuangzi.ad.util.ADConst.ERROR_CODE;
import static com.cn.shuangzi.ad.util.ADConst.ERROR_MSG;
import static com.cn.shuangzi.ad.util.ADConst.TAG_SPLASH;
import static com.cn.shuangzi.retrofit.SZRetrofitManager.ERROR_NET_FAILED;

/**
 * Created by CN.
 */

public class ADManager {
    private static ADManager INSTANCE;
    private SZXmlUtil sZXmlUtil;

    private ADHttpRequestRetrofitService adHttpRequestRetrofitService;
    private static String API_SERVICE = "https://switch.api.shuangzikeji.cn:8080/";
    private String deviceId;

    private List<OnAdInitDoneListener> onAdInitDoneListenerList;
    private boolean isInitAd;


    private UnifiedBannerView ylhBannerAd;
    private YlhErrorInfo bannerYlhErrorInfo;
    private SplashAD ylhSplashAd;
    private YlhErrorInfo splashYlhErrorInfo;
    private UnifiedInterstitialAD ylhInterstitialAd;
    private YlhErrorInfo interstitialYlhErrorInfo;
    private RewardVideoAD ylhRewardAd;
    private YlhErrorInfo rewardYlhErrorInfo;

    private KsSplashScreenAd ksSplashAd;
    private KsErrorInfo splashKsErrorInfo;
    private KsInterstitialAd ksInterstitialAd;
    private KsErrorInfo interstitialKsErrorInfo;
    private KsRewardVideoAd ksRewardAd;
    private KsErrorInfo rewardKsErrorInfo;
    private Map<String, Boolean> sdkInitMap;
    private int days = 1;

    private ADManager() {
        ADUtil.logError("实例化ADManager管理类");
        isInitAd = false;
        if (sZXmlUtil == null) {
            sZXmlUtil = new SZXmlUtil(SZManager.getInstance().getContext(), AD_LOAD_POLICY);
        }
        if (sdkInitMap == null) {
            sdkInitMap = new HashMap<>();
        } else {
            sdkInitMap.clear();
        }
        deviceId = sZXmlUtil.getString("device_id");
        if (TextUtils.isEmpty(deviceId)) {
            deviceId = ADUtil.getUUID();
            sZXmlUtil.put("device_id", deviceId);
        }
    }

    public static ADManager getInstance() {
        if (INSTANCE == null) {
            synchronized (ADManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ADManager();
                }
            }
        }
        return INSTANCE;
    }
    /**获取隔days天记录一次插屏广告已显示的时间戳，用来判断是否显示免广告等提示信息*/
    public int getAdIntervalDays() {
        return days;
    }

    /**设置隔days天记录一次插屏广告已显示的时间戳，用来判断是否显示免广告等提示信息*/
    public void setAdIntervalDays(int days) {
        this.days = days;
    }

    private boolean isGDTInitSuccess = false;

    public void initAd(boolean isGMAd) {
        Context context = SZManager.getInstance().getContext();
        if (context.getPackageName().equals(ADUtil.getCurrentProcessName(context))) {
            if (!isInitAd()) {
                ADUtil.logError("广告插件未初始化，开始初始化");
                setInitAd(true);
                TTAdManagerHolder.isBeginInit = false;
                TTAdManagerHolder.init(SZApp.getInstance(), SZManager.getInstance().isDebugMode(), onAdInitDoneListener, isGMAd);
                if (!isGMAd && !isGDTInitSuccess) {
                    if (hasADPlatform(ADConst.AD_PLATFORM_GDT)) {
                        SZUtil.logError("========非gromore初始化优量汇========");
                        GlobalSetting.setChannel(ADManager.getGDTChannelCode());
                        boolean isCanUsePhone = ADUtil.isCanUsePermission(Manifest.permission
                                .READ_PHONE_STATE);
                        GlobalSetting.setAgreeReadDeviceId(isCanUsePhone);
                        GlobalSetting.setAgreeReadAndroidId(isCanUsePhone);
                        GlobalSetting.setAgreePrivacyStrategy(isCanUsePhone);
                        GlobalSetting.setEnableCollectAppInstallStatus(isCanUsePhone);
                        GlobalSetting.setPersonalizedState(isCanUsePhone ? 0 : 1);//优量汇个性化推荐广告开关，0为开启个性化推荐广告，1为屏蔽个性化推荐广告
                        GDTAdSdk.initWithoutStart(context, ADApp.getInstance().getGDTAppId()); // 调用此接口进行初始化，该接口不会采集用户信息
                        // 调用initWithoutStart后请尽快调用start，否则可能影响广告填充，造成收入下降
                        GDTAdSdk.start(new GDTAdSdk.OnStartListener() {
                            @Override
                            public void onStartSuccess() {
                                isGDTInitSuccess = true;
                                SZUtil.logError("优量汇初始化成功");
                                // 推荐开发者在onStartSuccess回调后开始拉广告
                                //初始化成功回调
                            }

                            @Override
                            public void onStartFailed(Exception e) {
                                setInitAd(false);
                                isGDTInitSuccess = false;
                                SZUtil.logError("优量汇初始化失败：" + e.toString());
                            }
                        });
                    }
                }
            }
        }
    }

    public boolean isInitAd() {
        return isInitAd;
    }

    public void setInitAd(boolean initAd) {
        isInitAd = initAd;
    }

    public boolean isShowAd() {
        if (sZXmlUtil == null) {
            sZXmlUtil = new SZXmlUtil(SZManager.getInstance().getContext(), AD_LOAD_POLICY);
        }
        String data = sZXmlUtil.getString(AD_LOAD_SPLASH_POLICY);
        if (data != null && !data.equals("")) {
            try {
                List<ADPlatform> adPlatformList = new Gson().fromJson(data, new TypeToken<List<ADPlatform>>() {
                }.getType());
                if (adPlatformList != null && adPlatformList.size() > 0) {
                    return adPlatformList.get(0).isAdIsOpen();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public List<ADPlatform> getSplashAdList() {
        List<ADPlatform> adPlatformList = null;
        String data = sZXmlUtil.getString(AD_LOAD_SPLASH_POLICY);
        if (data != null && !data.equals("")) {
            try {
                adPlatformList = new Gson().fromJson(data, new TypeToken<List<ADPlatform>>() {
                }.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return adPlatformList;
    }

    public boolean hasADPlatform(String platform) {
        if (platform != null) {
            String data = sZXmlUtil.getString(AD_LOAD_SPLASH_POLICY);
            if (data != null && !data.equals("")) {
                try {
                    List<ADPlatform> adPlatformList = new Gson().fromJson(data, new TypeToken<List<ADPlatform>>() {
                    }.getType());
                    if (adPlatformList != null) {
                        for (ADPlatform adPlatform : adPlatformList) {
                            if (platform.equals(adPlatform.getAdSupportType())) {
                                return true;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public AdShowTypeInfo getSplashClickEyeAdShowType() {
        List<ADPlatform> adPlatformList = getSplashAdList();
        AdShowTypeInfo adShowTypeInfo = null;
        if (SZValidatorUtil.isValidList(adPlatformList)) {
            for (ADPlatform adPlatform : adPlatformList) {
                if (adPlatform.getAdShowTypeInfo() != null && SZValidatorUtil.isValidString(adPlatform.getAdShowTypeInfo().getClickEyeMode())) {
                    adShowTypeInfo = adPlatform.getAdShowTypeInfo();
                    break;
                }
            }
        }
        return adShowTypeInfo;
    }

    public boolean isShowGroMoreInteractionAd(boolean isShownSplashAd) {
        AdShowTypeInfo adShowTypeInfo = getInteractionAdShowType();
        if (adShowTypeInfo != null && SZValidatorUtil.isValidString(adShowTypeInfo.getShowType())) {
            switch (adShowTypeInfo.getShowType()) {
                case AdShowTypeInfo.ALWAYS_TYPE:
                    ADUtil.logError("=====总是显示插屏=====");
                    return true;
                case AdShowTypeInfo.CLOSE_TYPE:
                    ADUtil.logError("=====插屏广告位关闭=====");
                    return false;
                case AdShowTypeInfo.NORMAL_TYPE:
                    if (isShownSplashAd) {
                        return isShowGroMoreInteractionAdToday();
                    }
                    ADUtil.logError("=====未显示开屏，可显示插屏=====");
                    return true;
            }
            ADUtil.logError("插屏广告位类型为空，走默认是否显示插屏判断：" + !isShownSplashAd);
            return !isShownSplashAd;
        }
        ADUtil.logError("=====插屏广告位为空=====");
        return false;
    }

    private boolean isShowGroMoreInteractionAdToday() {
        Date date = new Date();
        String today = SZDateUtil.getSimpleYearMonthDateTime(date);
        SZXmlUtil szXmlUtil = new SZXmlUtil("gromore_interaction");
        long showTime1 = szXmlUtil.getLong(today + "_1");
        long showTime2 = szXmlUtil.getLong(today + "_2");
        long showTime3 = szXmlUtil.getLong(today + "_3");
        long showTime4 = szXmlUtil.getLong(today + "_4");
        long showTime5 = szXmlUtil.getLong(today + "_5");
        if (showTime1 == 0 || showTime2 == 0 || showTime3 == 0 || showTime4 == 0 || showTime5 == 0) {
            int hour = getHourByDate(date);
            if (hour > -1) {
                if (hour >= 6 && hour <= 22) {
                    if (showTime1 == 0) {
                        ADUtil.logError("可进行插屏第一次展示");
                        return true;
                    } else if (showTime2 == 0) {
                        if ((System.currentTimeMillis() - showTime1) >= (2 * SZDateUtil.ONE_HOUR)) {
                            ADUtil.logError("可进行插屏第二次展示");
                            return true;
                        } else {
                            ADUtil.logError("今日第二次插屏，间隔时间不足2小时，暂不显示插屏");
                        }
                    } else if (showTime3 == 0) {
                        if ((System.currentTimeMillis() - showTime2) >= (2 * SZDateUtil.ONE_HOUR)) {
                            ADUtil.logError("可进行插屏第三次展示");
                            return true;
                        } else {
                            ADUtil.logError("今日第三次插屏，间隔时间不足2小时，暂不显示插屏");
                        }
                    } else if (showTime4 == 0) {
                        if ((System.currentTimeMillis() - showTime3) >= (2 * SZDateUtil.ONE_HOUR)) {
                            ADUtil.logError("可进行插屏第四次展示");
                            return true;
                        } else {
                            ADUtil.logError("今日第四次插屏，间隔时间不足2小时，暂不显示插屏");
                        }
                    } else if (showTime5 == 0) {
                        if ((System.currentTimeMillis() - showTime4) >= (2 * SZDateUtil.ONE_HOUR)) {
                            ADUtil.logError("可进行插屏第五次展示");
                            return true;
                        } else {
                            ADUtil.logError("今日第五次插屏，间隔时间不足2小时，暂不显示插屏");
                        }
                    } else {
                        ADUtil.logError("不应该显示此log，今日已显示五次插屏，暂不显示插屏");
                    }
                } else {
                    ADUtil.logError("因为不在时间段而不显示");
                }
            } else {
                ADUtil.logError("======当前小时数 获取异常======");
            }
            return false;
        } else {
            ADUtil.logError("今日已显示五次插屏，暂不显示插屏");
        }
        return false;
    }

    private int getHourByDate(Date date) {
        String hourStr = SZDateUtil.getTimeSimpleShow(date).split(":")[0];
        int hour = -1;
        try {
            if (hourStr.length() == 2 && "0".equals(String.valueOf(hourStr.charAt(0)))) {
                hour = Integer.parseInt(String.valueOf(hourStr.charAt(1)));
            } else {
                hour = Integer.parseInt(hourStr);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hour;
    }

    public void setGroMoreInteractionAdTodayShown() {
        SZXmlUtil szXmlUtil = new SZXmlUtil("gromore_interaction");
        String today = SZDateUtil.getSimpleYearMonthDateTime(new Date());
        long showTime1 = szXmlUtil.getLong(today + "_1");
        long showTime2 = szXmlUtil.getLong(today + "_2");
        long showTime3 = szXmlUtil.getLong(today + "_3");
        long showTime4 = szXmlUtil.getLong(today + "_4");
        long showTime5 = szXmlUtil.getLong(today + "_5");
        if (showTime1 == 0) {
            szXmlUtil.clear();
            szXmlUtil.put(today + "_1", System.currentTimeMillis());
            ADUtil.logError("设置今天（" + today + "_1）第一次显示");
        } else if (showTime2 == 0) {
            szXmlUtil.put(today + "_2", System.currentTimeMillis());
            ADUtil.logError("设置今天（" + today + "_2）第二次显示");
        } else if (showTime3 == 0) {
            szXmlUtil.put(today + "_3", System.currentTimeMillis());
            ADUtil.logError("设置今天（" + today + "_3）第三次显示");
        } else if (showTime4 == 0) {
            szXmlUtil.put(today + "_4", System.currentTimeMillis());
            ADUtil.logError("设置今天（" + today + "_4）第四次显示");
        } else if (showTime5 == 0) {
            szXmlUtil.put(today + "_5", System.currentTimeMillis());
            ADUtil.logError("设置今天（" + today + "_5）第五次显示");
        }
    }

//    public boolean isShowInteractionAd(boolean isShownClickEye) {
//        AdShowTypeInfo adShowTypeInfo = getInteractionAdShowType();
//        if (adShowTypeInfo != null && adShowTypeInfo.getShowType() != null) {
//            switch (adShowTypeInfo.getShowType()) {
//                case AdShowTypeInfo.ALWAYS_TYPE:
//                    return true;
//                case AdShowTypeInfo.CLOSE_TYPE:
//                    return false;
//                case AdShowTypeInfo.NORMAL_TYPE:
//                    return !isShownClickEye;
//            }
//            return !isShownClickEye;
//        }
//        return false;
//    }

    public AdShowTypeInfo getInteractionAdShowType() {
        List<ADPlatform> adPlatformList = getInteractionAdList();
        if (SZValidatorUtil.isValidList(adPlatformList)) {
            return adPlatformList.get(0).getAdShowTypeInfo();
        }
        return null;
    }

    public static void setApiService(String apiService) {
        API_SERVICE = apiService;
    }

    public ADHttpRequestRetrofitService getADApiService() {
        if (adHttpRequestRetrofitService == null) {
            adHttpRequestRetrofitService = SZRetrofitManager.getInstance().getRetrofit(API_SERVICE, new ADRequestInterceptor()).create(ADHttpRequestRetrofitService.class);
        }
        return adHttpRequestRetrofitService;
    }

    public void getSelfAd(String channel, String packageName, String version, final OnSplashADLoadListener onSplashADLoadListener) {
        SZRetrofitManager.getInstance().request(getADApiService().getAd(channel, packageName, version, "ALL", deviceId), TAG_SPLASH, new SZRetrofitResponseListener.SimpleSZRetrofitResponseListener() {
            @Override
            public void onSuccess(String data) {
                List<ADPlatform> adPlatformList = new Gson().fromJson(data, new TypeToken<List<ADPlatform>>() {
                }.getType());
                List<ADPlatform> adSelfBannerList = new ArrayList<>();
                List<ADPlatform> adSelfFloatList = new ArrayList<>();
                if (SZValidatorUtil.isValidList(adPlatformList)) {
                    for (ADPlatform adPlatform : adPlatformList) {
                        switch (adPlatform.getAdType()) {
                            case ADPlatform.AD_TYPE_BANNER:
                                if (adPlatform.isSelfAd()) {
                                    adSelfBannerList.add(adPlatform);
                                }
                                break;
                            case ADPlatform.AD_TYPE_GIFT_BOX:
                                if (adPlatform.isSelfAd()) {
                                    adSelfFloatList.add(adPlatform);
                                }
                                break;
                        }
                    }
                }
                if (SZValidatorUtil.isValidList(adSelfBannerList)) {
                    sZXmlUtil.put(AD_LOAD_SELF_BANNER_POLICY, new Gson().toJson(adSelfBannerList));
                } else {
                    sZXmlUtil.remove(AD_LOAD_SELF_BANNER_POLICY);
                }
                if (SZValidatorUtil.isValidList(adSelfFloatList)) {
                    sZXmlUtil.put(AD_LOAD_SELF_FLOAT_POLICY, new Gson().toJson(adSelfFloatList));
                } else {
                    sZXmlUtil.remove(AD_LOAD_SELF_FLOAT_POLICY);
                }
                if (onSplashADLoadListener != null) {
                    onSplashADLoadListener.onFetchAdSuccess(null);
                }
            }

            @Override
            public void onNetError(int errorCode, String errorMsg) {
                SZManager.getInstance().onUMEvent(AD_LOCAL_ERROR_EVENT_ID, getErrorEventMap(AD_PLATFORM_LOCAL, String.valueOf(errorCode), errorMsg));
                ADUtil.logError("errorCode:" + errorCode + "|errorMsg:" + errorMsg);
                if (errorCode == 4022044) {
                    sZXmlUtil.remove(AD_LOAD_SPLASH_POLICY);
                    sZXmlUtil.remove(AD_LOAD_NATIVE_POLICY);
                }
                if (onSplashADLoadListener != null) {
                    if (errorCode == 502 || errorCode == ERROR_NET_FAILED) {
                        SZManager.getInstance().onUMEvent(AD_READ_LOCAL_EVENT_ID);
                    }
                    onSplashADLoadListener.onFetchAdError();
                }
            }
        });
    }

    public void getSplash(String channel, String packageName, String version, final OnSplashADLoadListener onSplashADLoadListener) {
        SZRetrofitManager.getInstance().request(getADApiService().getAd(channel, packageName, version, "ALL", deviceId), TAG_SPLASH, new SZRetrofitResponseListener.SimpleSZRetrofitResponseListener() {
            @Override
            public void onSuccess(String data) {
                List<ADPlatform> adPlatformList = new Gson().fromJson(data, new TypeToken<List<ADPlatform>>() {
                }.getType());

                List<ADPlatform> adSplashList = new ArrayList<>();
                List<ADPlatform> adNativeList = new ArrayList<>();
                List<ADPlatform> adSelfBannerList = new ArrayList<>();
                List<ADPlatform> adSelfFloatList = new ArrayList<>();
                List<ADPlatform> adInteractionList = new ArrayList<>();
                List<ADPlatform> adBannerList = new ArrayList<>();
                if (SZValidatorUtil.isValidList(adPlatformList)) {
                    for (ADPlatform adPlatform : adPlatformList) {
                        switch (adPlatform.getAdType()) {
                            case ADPlatform.AD_TYPE_SPLASH:
                                adSplashList.add(adPlatform);
                                break;
                            case ADPlatform.AD_TYPE_NATIVE:
                                adNativeList.add(adPlatform);
                                break;
                            case ADPlatform.AD_TYPE_BANNER:
                                if (adPlatform.isSelfAd()) {
                                    adSelfBannerList.add(adPlatform);
                                } else {
                                    adBannerList.add(adPlatform);
                                }
                                break;
                            case ADPlatform.AD_TYPE_GIFT_BOX:
                                if (adPlatform.isSelfAd()) {
                                    adSelfFloatList.add(adPlatform);
                                }
                                break;
                            case ADPlatform.AD_TYPE_INTERACTION:
                                adInteractionList.add(adPlatform);
                                break;
                        }
                    }
                }
                try {
                    sZXmlUtil.put(AD_LOAD_NATIVE_POLICY, new Gson().toJson(adNativeList));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (SZValidatorUtil.isValidList(adSelfBannerList)) {
                    sZXmlUtil.put(AD_LOAD_SELF_BANNER_POLICY, new Gson().toJson(adSelfBannerList));
                } else {
                    sZXmlUtil.remove(AD_LOAD_SELF_BANNER_POLICY);
                }
                if (SZValidatorUtil.isValidList(adBannerList)) {
                    sZXmlUtil.put(AD_LOAD_BANNER_POLICY, new Gson().toJson(adBannerList));
                } else {
                    sZXmlUtil.remove(AD_LOAD_BANNER_POLICY);
                }
                if (SZValidatorUtil.isValidList(adSelfFloatList)) {
                    sZXmlUtil.put(AD_LOAD_SELF_FLOAT_POLICY, new Gson().toJson(adSelfFloatList));
                } else {
                    sZXmlUtil.remove(AD_LOAD_SELF_FLOAT_POLICY);
                }
                if (SZValidatorUtil.isValidList(adInteractionList)) {
                    sZXmlUtil.put(AD_LOAD_INTERACTION_POLICY, new Gson().toJson(adInteractionList));
                } else {
                    sZXmlUtil.remove(AD_LOAD_INTERACTION_POLICY);
                }

                sZXmlUtil.put(AD_LOAD_SPLASH_POLICY, new Gson().toJson(adSplashList));
                if (onSplashADLoadListener != null) {
                    onSplashADLoadListener.onFetchAdSuccess(adSplashList);
                }
            }

            @Override
            public void onNetError(int errorCode, String errorMsg) {
                SZManager.getInstance().onUMEvent(AD_LOCAL_ERROR_EVENT_ID, getErrorEventMap(AD_PLATFORM_LOCAL, String.valueOf(errorCode), errorMsg));
                ADUtil.logError("errorCode:" + errorCode + "|errorMsg:" + errorMsg);
                if (errorCode == 4022044) {
                    sZXmlUtil.remove(AD_LOAD_SPLASH_POLICY);
                    sZXmlUtil.remove(AD_LOAD_NATIVE_POLICY);
                }
                if (onSplashADLoadListener != null) {
                    if (errorCode == 502 || errorCode == ERROR_NET_FAILED) {
                        SZManager.getInstance().onUMEvent(AD_READ_LOCAL_EVENT_ID);
                        String data = sZXmlUtil.getString(AD_LOAD_SPLASH_POLICY);
                        if (data != null && !data.equals("")) {
                            try {
                                List<ADPlatform> adPlatformList = new Gson().fromJson(data, new TypeToken<List<ADPlatform>>() {
                                }.getType());
                                onSplashADLoadListener.onFetchAdSuccess(adPlatformList);
                                return;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    onSplashADLoadListener.onFetchAdError();
                }
            }
        });
    }


    public List<ADPlatform> getInteractionAdList() {
        List<ADPlatform> adPlatformList = null;
        String interaction = sZXmlUtil.getString(AD_LOAD_INTERACTION_POLICY);
        if (SZValidatorUtil.isValidString(interaction)) {
            adPlatformList = new Gson().fromJson(interaction, new TypeToken<List<ADPlatform>>() {
            }.getType());
        }
        return adPlatformList;
    }

    public List<ADPlatform> getBannerAdList() {
        List<ADPlatform> adPlatformList = null;
        String banner = sZXmlUtil.getString(AD_LOAD_BANNER_POLICY);
        if (SZValidatorUtil.isValidString(banner)) {
            adPlatformList = new Gson().fromJson(banner, new TypeToken<List<ADPlatform>>() {
            }.getType());
        }
        return adPlatformList;
    }

    public boolean isShowBanner() {
        return SZValidatorUtil.isValidList(getBannerAdList());
    }

    public List<MaterialsInfo> getSelfBannerList() {
        List<MaterialsInfo> materialsInfoList = new ArrayList<>();
        try {
            String banner = sZXmlUtil.getString(AD_LOAD_SELF_BANNER_POLICY);
            if (SZValidatorUtil.isValidString(banner)) {
                List<ADPlatform> bannerList = new Gson().fromJson(banner, new TypeToken<List<ADPlatform>>() {
                }.getType());
                if (bannerList != null) {
                    for (ADPlatform adPlatform : bannerList) {
                        materialsInfoList.add(adPlatform.getMaterialsInfo());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return materialsInfoList;
    }

    public MaterialsInfo getSelfFloat() {
        try {
            String giftBox = sZXmlUtil.getString(AD_LOAD_SELF_FLOAT_POLICY);
            if (SZValidatorUtil.isValidString(giftBox)) {
                List<ADPlatform> floatList = new Gson().fromJson(giftBox, new TypeToken<List<ADPlatform>>() {
                }.getType());
                if (floatList != null) {
                    for (ADPlatform adPlatform : floatList) {
                        return adPlatform.getMaterialsInfo();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Map<String, String> getErrorEventMap(String platform, String errorCode, String errorMsg) {
        Map<String, String> params = new HashMap<>();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(ERROR_MSG, errorMsg);
            jsonObject.put(ERROR_CODE, errorCode);
            params.put(platform, jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    public UnifiedInterstitialAD getYlhInterstitialAD() {
        return ylhInterstitialAd;
    }

    public void setInterstitialYlhAD(UnifiedInterstitialAD currentUnifiedInterstitialAd) {
        this.ylhInterstitialAd = currentUnifiedInterstitialAd;
    }

    public SplashAD getYlhSplashAD() {
        return ylhSplashAd;
    }

    public void setYlhSplashAD(SplashAD ylhSplashAd) {
        this.ylhSplashAd = ylhSplashAd;
    }

    public UnifiedBannerView getYlhBannerAd() {
        return ylhBannerAd;
    }

    public void setYlhBannerAd(UnifiedBannerView currentUnifiedBannerView) {
        this.ylhBannerAd = currentUnifiedBannerView;
    }

    public YlhErrorInfo getBannerYlhErrorInfo() {
        return bannerYlhErrorInfo;
    }

    public void setBannerYlhErrorInfo(YlhErrorInfo bannerYlhErrorInfo) {
        this.bannerYlhErrorInfo = bannerYlhErrorInfo;
    }

    public YlhErrorInfo getSplashYlhErrorInfo() {
        return splashYlhErrorInfo;
    }

    public void setSplashYlhErrorInfo(YlhErrorInfo splashYlhErrorInfo) {
        this.splashYlhErrorInfo = splashYlhErrorInfo;
    }

    public YlhErrorInfo getInterstitialYlhErrorInfo() {
        return interstitialYlhErrorInfo;
    }

    public void setInterstitialYlhErrorInfo(YlhErrorInfo interstitialErrorInfo) {
        this.interstitialYlhErrorInfo = interstitialErrorInfo;
    }

    public RewardVideoAD getYlhRewardAd() {
        return ylhRewardAd;
    }

    public void setRewardYlhAd(RewardVideoAD ylhRewardAd) {
        this.ylhRewardAd = ylhRewardAd;
    }

    public YlhErrorInfo getRewardYlhErrorInfo() {
        return rewardYlhErrorInfo;
    }

    public void setRewardYlhErrorInfo(YlhErrorInfo rewardYlhErrorInfo) {
        this.rewardYlhErrorInfo = rewardYlhErrorInfo;
    }

    public KsErrorInfo getRewardKsErrorInfo() {
        return rewardKsErrorInfo;
    }

    public void setRewardKsErrorInfo(KsErrorInfo rewardKsErrorInfo) {
        this.rewardKsErrorInfo = rewardKsErrorInfo;
    }

    public KsRewardVideoAd getKsRewardAd() {
        return ksRewardAd;
    }

    public void setRewardKsAd(KsRewardVideoAd ksRewardAd) {
        this.ksRewardAd = ksRewardAd;
    }

    public boolean isYlhPlatform(String platform) {
        return "408".equals(platform);
    }

    public boolean isKsPlatform(String platform) {
        return "1862".equals(platform);
    }

    public @AdnName
    String getReportKsAdnName(String sdkName) {
        if (sdkName != null) {
            if (isYlhPlatform(sdkName)) {
                return AdnName.GUANGDIANTONG;
            }
        }
        return AdnName.CHUANSHANJIA;
    }

    public KsSplashScreenAd getKsSplashAd() {
        return ksSplashAd;
    }

    public void setKsSplashAd(KsSplashScreenAd ksSplashAd) {
        this.ksSplashAd = ksSplashAd;
    }

    public KsErrorInfo getSplashKsErrorInfo() {
        return splashKsErrorInfo;
    }

    public void setSplashKsErrorInfo(KsErrorInfo splashKsErrorInfo) {
        this.splashKsErrorInfo = splashKsErrorInfo;
    }

    public KsInterstitialAd getKsInterstitialAD() {
        return ksInterstitialAd;
    }

    public void setKsInterstitialAD(KsInterstitialAd ksInterstitialAd) {
        this.ksInterstitialAd = ksInterstitialAd;
    }

    public KsErrorInfo getInterstitialKsErrorInfo() {
        return interstitialKsErrorInfo;
    }

    public void setInterstitialKsErrorInfo(KsErrorInfo interstitialKsErrorInfo) {
        this.interstitialKsErrorInfo = interstitialKsErrorInfo;
    }

    public void clearCurrentSplash() {
        ylhSplashAd = null;
        ksSplashAd = null;
        splashYlhErrorInfo = null;
        splashKsErrorInfo = null;
    }

    public void clearCurrentInterstitial() {
        ylhInterstitialAd = null;
        ksInterstitialAd = null;
        interstitialYlhErrorInfo = null;
        interstitialKsErrorInfo = null;
    }

    public void clearCurrentReward() {
        ylhRewardAd = null;
        ksRewardAd = null;
        rewardYlhErrorInfo = null;
        rewardKsErrorInfo = null;
    }

    public static int getGDTChannelCode() {
        String channel = ADUtil.getChannel(SZManager.getInstance().getContext());
        int channelCode = 999;
//        1	百度
//        2	头条
//        3	优量汇
//        4	搜狗
//        5	其他网盟
//        6	oppo
//        7	vivo
//        8	华为
//        9	应用宝
//        10	小米
//        11	金立
//        12	百度手机助手
//        13	魅族
//        14	AppStore
//        999	其他
        if (SZValidatorUtil.isValidString(channel)) {
            switch (channel) {
                case "huawei":
                    channelCode = 8;
                    break;
                case "oppo":
                    channelCode = 6;
                    break;
                case "vivo":
                    channelCode = 7;
                    break;
                case "xiaomi":
                    channelCode = 10;
                    break;
                case "baidu":
                    channelCode = 1;
                    break;
                case "tencent":
                    channelCode = 9;
                    break;
                case "meizu":
                    channelCode = 13;
                    break;
                case "wandoujia":
                case "qihu360":
                    channelCode = 5;
                    break;
                case "sougou":
                    channelCode = 4;
                    break;
                case "toutiao":
                    channelCode = 2;
                    break;
                case "gdt":
                    channelCode = 3;
                    break;
            }
        }
        return channelCode;
    }

    public interface OnSplashADLoadListener {
        void onFetchAdSuccess(List<ADPlatform> adPlatformList);

        void onFetchAdError();
    }

    private OnAdInitDoneListener onAdInitDoneListener = new OnAdInitDoneListener() {
        @Override
        public void onTTAdInitDone(boolean isSuccess) {
            if (!isSuccess) {
                setInitAd(false);
            }
            if (onAdInitDoneListenerList != null) {
                for (OnAdInitDoneListener onAdInitDoneListenerChild : onAdInitDoneListenerList) {
                    if (onAdInitDoneListenerChild != null) {
                        onAdInitDoneListenerChild.onTTAdInitDone(isSuccess);
                    }
                }
            }
        }
    };

    public OnAdInitDoneListener getOnAdInitDoneListener() {
        return onAdInitDoneListener;
    }

    public List<OnAdInitDoneListener> getOnAdInitDoneListenerList() {
        return onAdInitDoneListenerList;
    }

    public void addOnAdInitDoneListener(OnAdInitDoneListener onAdInitDoneListener) {
        if (onAdInitDoneListenerList == null) {
            onAdInitDoneListenerList = new ArrayList<>();
        }
        onAdInitDoneListenerList.add(onAdInitDoneListener);
    }

    public void removeOnAdInitDoneListener(OnAdInitDoneListener onAdInitDoneListener) {
        if (onAdInitDoneListenerList != null) {
            onAdInitDoneListenerList.remove(onAdInitDoneListener);
        }
    }

    public void clearOnAdInitDoneListener() {
        if (onAdInitDoneListenerList != null) {
            onAdInitDoneListenerList.clear();
        }
    }

    public interface OnAdInitDoneListener {
        void onTTAdInitDone(boolean isSuccess);
    }

    private void setInitDone(String platform, boolean isSuccess) {
        if (sdkInitMap == null) {
            sdkInitMap = new HashMap<>();
        }
        sdkInitMap.put(platform, isSuccess);
        dealInitDone();
    }

    private void dealInitDone() {
        boolean isSuccessAll = false;
        Set<String> keySet = sdkInitMap.keySet();
        if (keySet != null && !keySet.isEmpty()) {
            Iterator<String> iterator = keySet.iterator();
            while (iterator.hasNext()) {
                String key = iterator.next();
                boolean isSuccess = sdkInitMap.get(key);
                if (!isSuccess) {
                    isSuccessAll = false;
                    break;
                }
            }
        }
        if(isSuccessAll) {
            List<OnAdInitDoneListener> listenerList = getOnAdInitDoneListenerList();
            if (listenerList != null && !listenerList.isEmpty()) {
                for (OnAdInitDoneListener adInitDoneListener : listenerList) {
                    adInitDoneListener.onTTAdInitDone(isSuccessAll);
                }
            }
        }
    }
}
