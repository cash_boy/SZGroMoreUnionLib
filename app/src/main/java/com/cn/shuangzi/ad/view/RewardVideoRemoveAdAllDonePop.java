package com.cn.shuangzi.ad.view;


import android.app.Activity;
import android.os.Build;
import android.view.View;

import com.cn.shuangzi.ad.R;
import com.cn.shuangzi.view.pop.BasePop;

/**
 * Created by CN.
 */
public class RewardVideoRemoveAdAllDonePop extends BasePop {

    public RewardVideoRemoveAdAllDonePop(Activity activity) {
        this(activity,false);
    }

    public RewardVideoRemoveAdAllDonePop(Activity activity, boolean isSetMaskerForeground) {
        super(activity, R.layout.pop_reward_video_remove_ad_all_done, true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isSetMaskerForeground) {
                findViewById(R.id.lltBg).setForeground(activity.getResources().getDrawable(R.drawable.fg_pop_reward_video_remove_ad_night));
            }
        }
        findViewById(R.id.imgClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissPop();
            }
        });
        findViewById(R.id.txtBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissPop();
            }
        });
    }


}
