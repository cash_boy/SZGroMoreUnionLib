package com.cn.shuangzi.ad.bean;

import com.kwad.sdk.api.model.AdExposureFailedReason;
import com.kwad.sdk.api.model.AdExposureFailureCode;

/**
 * Created by CN.
 */
public class KsErrorInfo {
    private AdExposureFailedReason adExposureFailedReason;
    private @AdExposureFailureCode int adExposureFailureCode;

    public KsErrorInfo(AdExposureFailedReason adExposureFailedReason) {
        this.adExposureFailedReason = adExposureFailedReason;
        this.adExposureFailureCode = AdExposureFailureCode.BID_FAILED;
    }

    public KsErrorInfo(@AdExposureFailureCode int adExposureFailureCode) {
        this.adExposureFailureCode = adExposureFailureCode;
    }

    public KsErrorInfo(AdExposureFailedReason adExposureFailedReason, int adExposureFailureCode) {
        this.adExposureFailedReason = adExposureFailedReason;
        this.adExposureFailureCode = adExposureFailureCode;
    }

    public AdExposureFailedReason getAdExposureFailedReason() {
        return adExposureFailedReason;
    }

    public void setAdExposureFailedReason(AdExposureFailedReason adExposureFailedReason) {
        this.adExposureFailedReason = adExposureFailedReason;
    }

    public int getAdExposureFailureCode() {
        return adExposureFailureCode;
    }

    public void setAdExposureFailureCode(int adExposureFailureCode) {
        this.adExposureFailureCode = adExposureFailureCode;
    }

    @Override
    public String toString() {
        String reason = null;
        if(adExposureFailedReason!=null) {
            reason = "{adnName = " + adExposureFailedReason.adnName +
                    "，adnType = " + adExposureFailedReason.adnType +
                    "，winEcpm = " + adExposureFailedReason.winEcpm +"}";
        };
        return "KsErrorInfo{" +
                "reason = "+reason+
                ", code = " + adExposureFailureCode +
                '}';
    }
}
