package com.cn.shuangzi.ad.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class AdShowTypeInfo implements Serializable{
    public static int NOT_SHOW_SECONDS = -1;
    public static final String NORMAL_MODE = "normal";
    public static final String CLOSE_MODE = "close";
    public static final String LAUNCH_MODE = "launch";
    public static final String ALWAYS_MODE = "always";

    public static final String ALWAYS_TYPE = "always";
    public static final String CLOSE_TYPE = "close";
    public static final String NORMAL_TYPE = "normal";
    private int lastSecond;
    private String showType;
    private String clickEyeMode;

    public boolean isShowClickEyeInLaunch(){
        return LAUNCH_MODE.equalsIgnoreCase(clickEyeMode);
    }

    public boolean isShowClickEyeNormal(){
        return NORMAL_MODE.equalsIgnoreCase(clickEyeMode);
    }
    public boolean isCloseClickEye(){
        return CLOSE_MODE.equalsIgnoreCase(clickEyeMode);
    }
    public boolean isShowClickEyeAlways(){
        return ALWAYS_MODE.equalsIgnoreCase(clickEyeMode);
    }

    public String getClickEyeMode() {
        return clickEyeMode;
    }
    public int getLastSecond() {
        return lastSecond;
    }

    public String getShowType() {
        return showType;
    }


}
