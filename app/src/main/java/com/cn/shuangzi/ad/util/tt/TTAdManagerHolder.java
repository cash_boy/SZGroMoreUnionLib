package com.cn.shuangzi.ad.util.tt;

import android.Manifest;
import android.content.Context;

import com.bytedance.sdk.openadsdk.TTAdConfig;
import com.bytedance.sdk.openadsdk.TTAdConstant;
import com.bytedance.sdk.openadsdk.TTAdManager;
import com.bytedance.sdk.openadsdk.TTAdSdk;
import com.bytedance.sdk.openadsdk.TTCustomController;
import com.cn.shuangzi.SZManager;
import com.cn.shuangzi.ad.ADApp;
import com.cn.shuangzi.ad.ADManager;
import com.cn.shuangzi.ad.util.ADUtil;
import com.cn.shuangzi.util.SZUtil;

public class TTAdManagerHolder {
    //    private static TTAdManager ttAdManager;
    public static boolean isBeginInit;
    public static boolean isGMAd;
    public static Boolean isCanUsePhoneState;
    public static Boolean isCanUsePrivacy;
    public static Boolean isCanUseLocation;
    public static boolean isInitSuccess;
    public static void init(Context context, boolean isDebug, final ADManager.OnAdInitDoneListener onAdInitDoneListener,boolean isGMAd) {
        if(isBeginInit){
            return;
        }
        if(TTAdSdk.isInitSuccess()){
            isInitSuccess = true;
            ADUtil.logError("==========已经初始化成功，无需初始化=========");
            return;
        }
        isInitSuccess = false;
        TTAdManagerHolder.isGMAd = isGMAd;
        isCanUsePhoneState = null;
        isCanUsePrivacy = null;
        isCanUseLocation = null;
        isBeginInit = true;
        TTAdConfig.Builder builder = new TTAdConfig.Builder()
                .customController(new TTCustomController(){
                    public boolean isCanUsePhoneState() {
                        if(isCanUsePhoneState == null) {
                            isCanUsePhoneState = ADUtil.isCanUsePermission(Manifest.permission
                                    .READ_PHONE_STATE);
                        }
//                        ADUtil.logError("穿山甲-是否可使用读取设备权限：" + isCanUsePhoneState);
                        return isCanUsePhoneState;
                    }
                    @Override
                    public boolean alist() {
                        if(isCanUsePrivacy == null) {
                            isCanUsePrivacy = !ADUtil.isFirstDayUseAd();
                        }
//                        ADUtil.logError("穿山甲-是否可读取app列表：" + isCanUsePrivacy);
                        return isCanUsePrivacy;
                    }
                    @Override
                    public boolean isCanUseLocation() {
                        if(isCanUseLocation == null) {
                            isCanUseLocation = ADUtil.isCanUsePermission(Manifest.permission
                                    .ACCESS_FINE_LOCATION);
                        }
//                        ADUtil.logError("穿山甲-是否可使用定位权限："+isCanUseLocation);
                        return isCanUseLocation;
                    }

                    @Override
                    public boolean isCanUseAndroidId() {
                        if (isCanUsePrivacy == null) {
                            isCanUsePrivacy = !ADUtil.isFirstDayUseAd();
                        }
//                        ADUtil.logError("穿山甲-是否可读取AndroidId：" + isCanUsePrivacy);
                        return isCanUsePrivacy;
                    }

                    @Override
                    public boolean isCanUseWifiState() {
                        if (isCanUsePrivacy == null) {
                            isCanUsePrivacy = !ADUtil.isFirstDayUseAd();
                        }
//                        ADUtil.logError("穿山甲-是否可读取wifi状态：" + isCanUsePrivacy);
                        return isCanUsePrivacy;
                    }
                })
                .appId(ADApp.getInstance().getTTAppId())
                .appName(ADApp.getInstance().getADPlatformAppName())
                .useMediation(isGMAd)
                .directDownloadNetworkType()//允许直接下载的网络状态集合
//                .useTextureView(false) //使用TextureView控件播放视频,默认为SurfaceView,当有SurfaceView冲突的场景，可以使用TextureView
//                .titleBarTheme(TTAdConstant.TITLE_BAR_THEME_DARK)
                .allowShowNotify(true) //是否允许sdk展示通知栏提示
                .debug(isDebug) //测试阶段打开，可以通过日志排查问题，上线时去除该调用
                .supportMultiProcess(true)
                ;
        ADUtil.logError("穿山甲版本号："+TTAdSdk.getAdManager().getSDKVersion());

        //setp1.1：初始化SDK
        TTAdSdk.init(context, builder.build());
        //setp1.2：启动SDK
        TTAdSdk.start(new TTAdSdk.Callback() {
            @Override
            public void success() {
                isBeginInit = false;
                isInitSuccess = true;
                if(onAdInitDoneListener!=null){
                    onAdInitDoneListener.onTTAdInitDone(true);
                }
                ADUtil.logError("穿山甲初始化成功: " + TTAdSdk.isInitSuccess());
            }

            @Override
            public void fail(int code, String msg) {
                isBeginInit = false;
                isInitSuccess = false;
                if(onAdInitDoneListener!=null){
                    onAdInitDoneListener.onTTAdInitDone(false);
                }
                ADUtil.logError("穿山甲初始化失败:  code = " + code + " msg = " + msg);
            }
        });
    }

    public static boolean isInitSuccess() {
        return isInitSuccess;
    }

    public static TTAdManager getInstance() {
        if (!TTAdSdk.isSdkReady()) {
            init(SZManager.getInstance().getContext(), SZManager.getInstance().isDebugMode(),ADManager.getInstance().getOnAdInitDoneListener(),isGMAd);
        }
        return TTAdSdk.getAdManager();
    }
//    public static TTAdManager getInstance() {
//        if (!TTAdSdk.isInitSuccess()) {
//            synchronized (TTAdManagerHolder.class) {
//                    init(SZManager.getInstance().getContext(),SZManager.getInstance().isDebugMode(),isAutoDownloadInWifi);
//                }
//            }
//        }
//        return ttAdManager;
//    }
}
