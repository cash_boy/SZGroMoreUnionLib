package com.cn.shuangzi.ad.bean;


/**
 * Created by CN.
 */
public class YlhErrorInfo {
    private int reason;
    private String adnId = "2";//1 - 输给优量汇其它广告；2 - 输给第三方ADN（无需回传具体竞胜方渠道）；3 - 输给自售广告主；

    public YlhErrorInfo(int reason) {
        this.reason = reason;
    }


    public int getReason() {
        return reason;
    }

    public void setReason(int reason) {
        this.reason = reason;
    }

    public String getAdnId() {
        return adnId;
    }

    public void setAdnId(String adnId) {
        this.adnId = adnId;
    }

    @Override
    public String toString() {
        return "YlhErrorInfo{" +
                "reason=" + reason +
                ", adnId='" + adnId + '\'' +
                '}';
    }
}
