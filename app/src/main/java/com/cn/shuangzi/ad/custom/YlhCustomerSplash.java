package com.cn.shuangzi.ad.custom;

import android.content.Context;
import android.os.SystemClock;
import android.view.ViewGroup;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.mediation.MediationConstant;
import com.bytedance.sdk.openadsdk.mediation.bridge.custom.splash.MediationCustomSplashLoader;
import com.bytedance.sdk.openadsdk.mediation.custom.MediationCustomServiceConfig;
import com.cn.shuangzi.ad.ADManager;
import com.cn.shuangzi.ad.bean.YlhErrorInfo;
import com.cn.shuangzi.ad.util.ADConst;
import com.cn.shuangzi.ad.util.ADUtil;
import com.cn.shuangzi.ad.util.ThreadUtils;
import com.qq.e.ads.splash.SplashAD;
import com.qq.e.ads.splash.SplashADListener;
import com.qq.e.comm.constants.BiddingLossReason;
import com.qq.e.comm.util.AdError;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * YLH 开屏广告自定义Adapter
 */
public class YlhCustomerSplash extends MediationCustomSplashLoader {

    private volatile SplashAD mSplashAD;

    private int price;

    @Override
    public void showAd(final ViewGroup container) {
        ADUtil.logError("=======显示优量汇开屏=======");
        if (mSplashAD!=null&&price > 0) {
            mSplashAD.sendWinNotification(price);
            ADUtil.logError("=======上报优量汇开屏数据======="+price);
        }
        /**
         * 先切子线程，再在子线程中切主线程进行广告展示
         */
        ThreadUtils.runOnUIThreadByThreadPool(new Runnable() {
            @Override
            public void run() {
                if (mSplashAD != null && container != null) {
                    container.removeAllViews();

                    if (isServerBidding()) {
                        mSplashAD.setBidECPM(mSplashAD.getECPM());
                    }
                    mSplashAD.showAd(container);
                }
            }
        });
    }

    @Override
    public void load(final Context context, AdSlot adSlot, final MediationCustomServiceConfig mediationCustomServiceConfig) {
        /**
         * 在子线程中进行广告加载
         */
        ThreadUtils.runOnThreadPool(new Runnable() {
            @Override
            public void run() {
                SplashADListener splashADListener = new SplashADListener() {
                    @Override
                    public void onADDismissed() {
                        ADUtil.logError("优量汇开屏onADDismissed");
                        callSplashAdDismiss();
                    }

                    @Override
                    public void onNoAD(AdError adError) {

                        YlhErrorInfo ylhErrorInfo;
                        if (adError != null) {
                            ylhErrorInfo = new YlhErrorInfo(BiddingLossReason.NO_AD);
                            ADUtil.logError("优量汇开屏加载失败-Splash onNoAD errorCode = " + adError.getErrorCode() + " errorMessage = " + adError.getErrorMsg());
                            callLoadFail(adError.getErrorCode(), adError.getErrorMsg());
                        } else {
                            ADUtil.logError("优量汇开屏加载失败-无错误信息");
                            ylhErrorInfo = new YlhErrorInfo(BiddingLossReason.NO_AD);
                            callLoadFail(ADConst.LOAD_ERROR, "no ad");
                        }
                        ADManager.getInstance().setSplashYlhErrorInfo(ylhErrorInfo);
                    }

                    @Override
                    public void onADPresent() {
                        ADUtil.logError("优量汇开屏onADPresent");
                    }

                    @Override
                    public void onADClicked() {
                        ADUtil.logError("优量汇开屏onADClicked");
                        callSplashAdClicked();
                    }

                    @Override
                    public void onADTick(long l) {
                    }

                    @Override
                    public void onADExposure() {
                        ADUtil.logError("优量汇开屏展示onADExposure");
                        callSplashAdShow();
                    }

                    @Override
                    public void onADLoaded(long expireTimestamp) {
                        ADUtil.logError("优量汇开屏加载完成 expireTimestamp："+expireTimestamp);
                        long timeIntervalSec = expireTimestamp - SystemClock.elapsedRealtime();
                        if (timeIntervalSec > 1000) {
                            if (isClientBidding()) {//bidding类型广告
                                double ecpm = mSplashAD.getECPM(); //当无权限调用该接口时，SDK会返回错误码-1
                                ADUtil.logError("优量汇开屏竞价："+ecpm);
                                if (ecpm < 0) {
                                    ecpm = 0;
                                }
                                price = (int) ecpm;
                                ADManager.getInstance().setSplashYlhErrorInfo(new YlhErrorInfo(BiddingLossReason.LOW_PRICE));
                                callLoadSuccess(ecpm);  //bidding广告成功回调，回传竞价广告价格
                            } else { //普通类型广告
                                callLoadSuccess();
                            }
                        } else {
                            ADUtil.logError("优量汇开屏失效");
                            callLoadFail(ADConst.LOAD_ERROR, "ad has expired");
                        }
                    }
                };
                if (isServerBidding()) {
                    mSplashAD = new SplashAD(context, mediationCustomServiceConfig.getADNNetworkSlotId(), splashADListener, 3000, getAdm());
                } else {
                    mSplashAD = new SplashAD(context, mediationCustomServiceConfig.getADNNetworkSlotId(), splashADListener, 3000);
                }
                mSplashAD.fetchAdOnly();
                ADManager.getInstance().setYlhSplashAD(mSplashAD);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSplashAD = null;
    }

    @Override
    public MediationConstant.AdIsReadyStatus isReadyCondition() {

        /**
         * 在子线程中进行广告是否可用的判断
         */
        Future<MediationConstant.AdIsReadyStatus> future = ThreadUtils.runOnThreadPool(new Callable<MediationConstant.AdIsReadyStatus>() {
            @Override
            public MediationConstant.AdIsReadyStatus call() throws Exception {
                if (mSplashAD != null && mSplashAD.isValid()) {
                    return MediationConstant.AdIsReadyStatus.AD_IS_READY;
                } else {
                    return MediationConstant.AdIsReadyStatus.AD_IS_NOT_READY;
                }
            }
        });
        try {
            MediationConstant.AdIsReadyStatus result = future.get(500, TimeUnit.MILLISECONDS);//设置500毫秒的总超时，避免线程阻塞
            if (result != null) {
                return result;
            } else {
                return MediationConstant.AdIsReadyStatus.AD_IS_NOT_READY;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return MediationConstant.AdIsReadyStatus.AD_IS_NOT_READY;
    }

    /**
     * 是否clientBidding广告
     *
     * @return
     */
    public boolean isClientBidding() {
        return getBiddingType() == MediationConstant.AD_TYPE_CLIENT_BIDING;
    }

    /**
     * 是否serverBidding广告
     *
     * @return
     */
    public boolean isServerBidding() {
        return getBiddingType() == MediationConstant.AD_TYPE_SERVER_BIDING;
    }

}
