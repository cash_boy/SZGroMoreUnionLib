package com.cn.shuangzi.ad.util;

import android.text.TextUtils;

import com.bytedance.sdk.openadsdk.mediation.MediationConstant;
import com.bytedance.sdk.openadsdk.mediation.ad.MediationSplashRequestInfo;
import com.cn.shuangzi.ad.ADApp;

public class SplashUtils {
    public static MediationSplashRequestInfo getGMNetworkRequestInfo(String bottomCodeId) {
        if(TextUtils.isEmpty(bottomCodeId)){
            return null;
        }
        MediationSplashRequestInfo networkRequestInfo = new MediationSplashRequestInfo(MediationConstant.ADN_PANGLE,bottomCodeId, ADApp.getInstance().getTTAppId(), ""){};
        return networkRequestInfo;
    }
}
