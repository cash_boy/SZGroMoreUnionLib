package com.cn.shuangzi.ad.custom;

import android.Manifest;
import android.content.Context;
import android.location.Location;

import com.bytedance.sdk.openadsdk.mediation.bridge.custom.MediationCustomInitLoader;
import com.bytedance.sdk.openadsdk.mediation.custom.MediationCustomInitConfig;
import com.cn.shuangzi.ad.ADApp;
import com.cn.shuangzi.ad.ADManager;
import com.cn.shuangzi.ad.util.ADUtil;
import com.cn.shuangzi.ad.util.ThreadUtils;
import com.cn.shuangzi.util.SZUtil;
import com.kwad.sdk.api.KsAdSDK;
import com.kwad.sdk.api.KsCustomController;
import com.kwad.sdk.api.KsInitCallback;
import com.kwad.sdk.api.SdkConfig;

import java.security.Permissions;
import java.util.Map;

import cc.shinichi.library.tool.ui.ToastUtil;

public class KsCustomerConfig extends SZMediationCustomInitLoader {

    @Override
    public void initializeADN(final Context context, final MediationCustomInitConfig customInitConfig, Map<String, Object> localExtra) {
        SZUtil.logError("快手sdk判断是否初始化："+isInit());
        SZUtil.logError("初始化成功："+isInitSuccess);
        SZUtil.logError("开始初始化："+isBeginInit);
        if (!isInit()&&!isInitSuccess&&!isBeginInit) {
            /**
             * 在子线程中进行初始化
             */
            ThreadUtils.runOnThreadPool(new Runnable() {
                @Override
                public void run() {
//                    KsAdSDK.init(context, new SdkConfig.Builder()
//                            .appId(customInitConfig.getAppId())
//                            .build());
//                    callInitSuccess();
                    SZUtil.logError("开始初始化快手，当前进程："+ADUtil.getCurrentProcessName(context));
                    SZUtil.logError("当前KsCustomerConfig："+KsCustomerConfig.this);

                    final boolean isCanUsePhone = ADUtil.isCanUsePermission(Manifest.permission
                            .READ_PHONE_STATE);
                    final boolean isFirstDayUseAd = ADUtil.isFirstDayUseAd();
                    KsAdSDK.init(context, new SdkConfig.Builder()
                            .appId(customInitConfig.getAppId()) // 测试aapId，请联系快手平台申请正式AppId，必填
//                            .showNotification(true) // 是否展示下载通知栏，非必填
                            .customController(new KsCustomController() {
                                @Override
                                public boolean canReadLocation() {
                                    return false;
                                }
                                @Override
                                public boolean canUsePhoneState() {
                                    return isCanUsePhone;
                                }
                                @Override
                                public boolean canReadInstalledPackages() {
                                    return !isFirstDayUseAd;
                                }
                            })// 控制SDK获取用户设备信息接口，非必填
                            .debug(ADApp.getInstance().isDebug())
//                            .setInitCallback(new KsInitCallback() {
//                                @Override
//                                public void onSuccess() {
//                                    SZUtil.logError("快手初始化成功！");
//                                }
//
//                                @Override
//                                public void onFail(int code, String msg) {
//                                    SZUtil.logError("快手初始化失败：code="+code+"|msg="+msg);
//                                }
//                            })
                            .setStartCallback(new KsInitCallback() {
                                @Override
                                public void onSuccess() {
                                    isBeginInit = false;
                                    isInitSuccess = true;
                                    SZUtil.logError("Start快手开始初始化成功！");
                                    callInitSuccess();
                                }

                                @Override
                                public void onFail(int code, String msg) {
                                    isInitSuccess = false;
                                    isBeginInit = false;
                                    SZUtil.logError("Start快手开始初始化失败：code="+code+"|msg="+msg);
                                }
                            })
                            .build());
                    KsAdSDK.setPersonalRecommend(!isFirstDayUseAd);
                    KsAdSDK.start();
                }
            });
        }
    }

    @Override
    public String getBiddingToken(Context context, Map<String, Object> extra) {
        ADUtil.logError("getBiddingToken extra = " + extra);
        return "bidding token";
    }

    @Override
    public String getSdkInfo(Context context, Map<String, Object> extra) {
        ADUtil.logError("快手 getSdkInfo extra = " + extra);
        return "sdk info";
    }

    @Override
    public String getNetworkSdkVersion() {
        return KsAdSDK.getSDKVersion();
    }
}
