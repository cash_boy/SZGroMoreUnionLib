package com.cn.shuangzi.ad.bean;

import com.cn.shuangzi.ad.ADApp;
import com.cn.shuangzi.util.SZDateUtil;
import com.cn.shuangzi.util.SZUtil;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by CN.
 */
public class RemoveAdRewardVideoTaskInfo implements Serializable {
    private long duration;//免广告时长
    private long endTime;//免广告结束时间
    private long doneTime;//任务完成时间
    private int taskId;

    public RemoveAdRewardVideoTaskInfo(long duration, int taskId) {
        this.duration = duration;
        this.taskId = taskId;
    }

    public RemoveAdRewardVideoTaskInfo(long duration, long endTime, long doneTime, int taskId) {
        this.duration = duration;
        this.endTime = endTime;
        this.doneTime = doneTime;
        this.taskId = taskId;
    }

    public boolean isTimeOver() {
        return System.currentTimeMillis() > endTime;
    }

    public long getTimeOverLong() {
        return System.currentTimeMillis() - endTime;
    }

    public long getTimeSurplusLong() {
        return endTime - System.currentTimeMillis();
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getEndTime() {
        return endTime;
    }

    public long getDoneTime() {
        return doneTime;
    }

    public void setDoneTime(long doneTime) {
        this.doneTime = doneTime;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public boolean equals(RemoveAdRewardVideoTaskInfo taskInfo) {
        if (this == taskInfo) {
            return true;
        }
        if (taskInfo == null) {
            return false;
        }
        return duration == taskInfo.duration &&
                taskId == taskInfo.taskId;
    }

    public void logInfo(){
        if(ADApp.getInstance().isDebug()) {
            SZUtil.logError("免广告到期时间：" + SZDateUtil.getShowDateAll(new Date(getEndTime())));
            SZUtil.logError("任务是否已超时：" + isTimeOver());
            if (isTimeOver()) {
                SZUtil.logError("需要显示广告，免广告超时时长：" + (getTimeOverLong() / SZDateUtil.ONE_MINUTE) + "分钟");
            } else {
                SZUtil.logError("不可显示广告，免广告剩余时长：" + (getTimeSurplusLong() / SZDateUtil.ONE_MINUTE) + "分钟");
            }
            SZUtil.logError("免广告任务时长：" + (getDuration() / SZDateUtil.ONE_MINUTE) + "分钟");
        }
    }
}
