package com.cn.shuangzi.ad.util;

import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.ad.ADApp;
import com.cn.shuangzi.ad.ADManager;
import com.cn.shuangzi.ad.bean.RemoveAdRewardVideoTaskInfo;
import com.cn.shuangzi.ad.view.AlertRemoveAdDialog;
import com.cn.shuangzi.ad.view.RewardVideoRemoveAdAllDonePop;
import com.cn.shuangzi.ad.view.RewardVideoRemoveAdPop;
import com.cn.shuangzi.common.SZConst;
import com.cn.shuangzi.util.SZArithUtil;
import com.cn.shuangzi.util.SZDateUtil;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.cn.shuangzi.util.SZXmlUtil;
import com.google.gson.Gson;

import java.util.Date;

/**
 * Created by CN.
 */
public class ADUtil extends SZUtil {
    private static final String AD = "ad";
    private static final String USE_TIME = "use_time";


    public static boolean isFirstShowInteractionAd() {
        return new SZXmlUtil(SZConst.SETTING).getBoolean("is_first_show_interaction", true);
    }

    public static void setShownInteractionAd() {
        SZXmlUtil szXmlUtil = new SZXmlUtil(SZConst.SETTING);
        szXmlUtil.put("is_first_show_interaction", false);
    }

    /**每隔days天记录一次显示插屏广告的时间*/
    public static void setShownInteractionAdByDays() {
        SZXmlUtil szXmlUtil = new SZXmlUtil(SZConst.SETTING);
        long time = szXmlUtil.getLong("show_interaction_day");
        if((System.currentTimeMillis()-time)>(SZDateUtil.ONE_DAY*ADManager.getInstance().getAdIntervalDays())){
            szXmlUtil.put("show_interaction_day", System.currentTimeMillis());
        }
    }

    public static Class<?> getBuyVipClass(Class<?> classBuyVip) {
        Class<?> classResult = isFirstShowInteractionAd() ? classBuyVip : null;
        return classResult;
    }

    public static Class<?> getBuyVipClassByDays(Class<?> classBuyVip) {
        long time = new SZXmlUtil(SZConst.SETTING).getLong("show_interaction_day");
        Class<?> classResult = ((System.currentTimeMillis()-time)>(SZDateUtil.ONE_DAY*ADManager.getInstance().getAdIntervalDays())) ? classBuyVip : null;
        return classResult;
    }

    public static boolean isFirstShowInteractionAdWithRemoveAdPop() {
        return new SZXmlUtil(SZConst.SETTING).getBoolean("first_interaction_with_remove", true);
    }

    public static void setShownInteractionAdWithRemoveAdPop() {
        new SZXmlUtil(SZConst.SETTING).put("first_interaction_with_remove", false);
    }

    public static AlertRemoveAdDialog.OnClickRemoveAdListener getOnClickRemoveAdListener(AlertRemoveAdDialog.OnClickRemoveAdListener onClickRemoveAdListener) {
        AlertRemoveAdDialog.OnClickRemoveAdListener  listenerResult = isFirstShowInteractionAdWithRemoveAdPop() ? onClickRemoveAdListener : null;
        return listenerResult;
    }
    /**是否为显示插屏后的第days天*/
    public static boolean isDaysAfterShowInteractionAd(){
        long time = new SZXmlUtil(SZConst.SETTING).getLong("show_interaction_day");
        return (System.currentTimeMillis()-time)>(SZDateUtil.ONE_DAY*ADManager.getInstance().getAdIntervalDays());
    }
    public static AlertRemoveAdDialog.OnClickRemoveAdListener getOnClickRemoveAdListenerByDays(AlertRemoveAdDialog.OnClickRemoveAdListener onClickRemoveAdListener) {
        long time = new SZXmlUtil(SZConst.SETTING).getLong("show_interaction_day");
        AlertRemoveAdDialog.OnClickRemoveAdListener  listenerResult = ((System.currentTimeMillis()-time)>(SZDateUtil.ONE_DAY*ADManager.getInstance().getAdIntervalDays())) ? onClickRemoveAdListener : null;
        return listenerResult;
    }

    public static boolean isFirstDayUseAd() {
        long time = new SZXmlUtil(AD).getLong(USE_TIME, 0);
        return isFirstDayUseAd(time);
    }

    public static boolean isFirstDayUseAd(long usedTime) {
        if (usedTime == 0) {
            return true;
        }
        long interval = System.currentTimeMillis() - usedTime;
        if (interval > SZDateUtil.ONE_DAY) {
            return false;
        }
        return true;
    }

    public static void setFirstUseAdTime() {
        SZXmlUtil xmlUtil = new SZXmlUtil(AD);
        long time = xmlUtil.getLong(USE_TIME, 0);
        if (time == 0 || time > System.currentTimeMillis()) {
            xmlUtil.put(USE_TIME, System.currentTimeMillis());
        }
    }

    public static boolean isCanUsePermission(String permission) {
        if (hasPermission(ADApp.getInstance().getApplicationContext(), permission)) {
            if (!isFirstDayUseAd()) {
                return true;
            }
        }
        return false;
    }

    public static boolean isResetRewardTask(RemoveAdRewardVideoTaskInfo taskInfo){
        if(taskInfo!=null){
            if(taskInfo.isTimeOver()){
                if(taskInfo.getTaskId() == RewardVideoRemoveAdPop.TASK_ID_5){
                    return true;
                }
                return (System.currentTimeMillis() - taskInfo.getEndTime()) > 3*SZDateUtil.ONE_HOUR;
            }
        }
        return false;
    }
    //通过去广告任务判断是否显示广告
    public static boolean isShowAdWithRewardTask(){
        RemoveAdRewardVideoTaskInfo taskInfo = getCurrentRemoveAdRewardVideoTaskInfo();
        if(taskInfo!=null){
            taskInfo.logInfo();
            return taskInfo.isTimeOver();
        }
        SZUtil.logError("没有已完成的任务，需要显示广告");
        return true;
    }

    public static boolean isShowRemoveAdTask(boolean isNeedToast,boolean isLogin,boolean isVip,boolean isNightMode) {
        if(!isLogin){
            SZUtil.logError("因为没有登录，所以不显示免广告任务");
            return false;
        }
        if(!canShowAd()){
            SZUtil.logError("因为是新用户，所以不显示免广告任务");
            return false;
        }
        if(!ADManager.getInstance().isShowAd()){
            SZUtil.logError("因为没有配置广告，所以不显示免广告任务");
            return false;
        }
        if (isVip) {
            if(isNeedToast){
                SZToast.infoLong("您已是VIP会员，无需完成去广告任务！");
            }
            return false;
        }
        RemoveAdRewardVideoTaskInfo taskInfo = getCurrentRemoveAdRewardVideoTaskInfo();
        if (taskInfo != null) {
            if ((taskInfo.getTaskId() == RewardVideoRemoveAdPop.TASK_ID_5)) {
                boolean isShow = taskInfo.isTimeOver();
                if(isShow){
                    if(isResetRewardTask(taskInfo)) {
                        clearCurrentRemoveAdRewardVideoTaskInfo();
                    }
                }else{
                    if(isNeedToast) {
                        new RewardVideoRemoveAdAllDonePop(ADApp.getInstance().getTopActivity(),isNightMode).showAsDropDown(((SZBaseActivity) ADApp.getInstance().getTopActivity()).getBaseView());
                    }
                }
                return isShow;
            }
        }
        return true;
    }

    public static void setCurrentRemoveAdRewardVideoTaskInfo(int hour) {
        RemoveAdRewardVideoTaskInfo taskInfo = getCurrentRemoveAdRewardVideoTaskInfo();
        long now = System.currentTimeMillis();
        long durationAdd = (long) (SZArithUtil.mul(hour, SZDateUtil.ONE_HOUR));
        long endTime = -1;
        if (taskInfo != null) {
            //如有已完成的任务，当前免广告时长减去已免广告时长，得出还需免广告的时长
            durationAdd = durationAdd - taskInfo.getDuration();
            endTime = taskInfo.getEndTime();
        }

        if (durationAdd <= 0) {
            durationAdd = (long) SZArithUtil.mul(hour, SZDateUtil.ONE_HOUR);
        }
        if (endTime > now) {
            endTime = endTime + durationAdd;
        } else {
            endTime = now + durationAdd;
        }
        setCurrentRemoveAdRewardVideoTaskInfo(new RemoveAdRewardVideoTaskInfo((long) SZArithUtil.mul(hour, SZDateUtil.ONE_HOUR),
                endTime, System.currentTimeMillis(), hour));

    }

    public static void clearCurrentRemoveAdRewardVideoTaskInfo() {
        new SZXmlUtil(SZConst.SETTING).remove("remove_ad_task");
    }

    public static void setCurrentRemoveAdRewardVideoTaskInfo(RemoveAdRewardVideoTaskInfo taskInfo) {
        new SZXmlUtil(SZConst.SETTING).put("remove_ad_task", new Gson().toJson(taskInfo));
    }

    public static RemoveAdRewardVideoTaskInfo getCurrentRemoveAdRewardVideoTaskInfo() {
        String taskStr = new SZXmlUtil(SZConst.SETTING).getString("remove_ad_task");
        if (SZValidatorUtil.isValidString(taskStr)) {
            return new Gson().fromJson(taskStr, RemoveAdRewardVideoTaskInfo.class);
        }
        return null;
    }

    public static RemoveAdRewardVideoTaskInfo getNoAdRewardVideoTaskInfo(int hour) {
        return new RemoveAdRewardVideoTaskInfo((long) SZArithUtil.mul(hour, SZDateUtil.ONE_HOUR), hour);
    }
}
