package com.cn.shuangzi.ad.custom;

import android.app.Activity;
import android.content.Context;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.mediation.MediationConstant;
import com.bytedance.sdk.openadsdk.mediation.bridge.custom.interstitial.MediationCustomInterstitialLoader;
import com.bytedance.sdk.openadsdk.mediation.custom.MediationCustomServiceConfig;
import com.cn.shuangzi.ad.ADManager;
import com.cn.shuangzi.ad.bean.YlhErrorInfo;
import com.cn.shuangzi.ad.util.ADConst;
import com.cn.shuangzi.ad.util.ADUtil;
import com.cn.shuangzi.ad.util.ThreadUtils;
import com.qq.e.ads.interstitial2.UnifiedInterstitialAD;
import com.qq.e.ads.interstitial2.UnifiedInterstitialADListener;
import com.qq.e.comm.constants.BiddingLossReason;
import com.qq.e.comm.util.AdError;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * YLH 插屏广告自定义Adapter
 */
public class YlhCustomerInterstitial extends MediationCustomInterstitialLoader {

    private volatile UnifiedInterstitialAD mUnifiedInterstitialAD;
    private int price;

    @Override
    public void showAd(final Activity activity) {

        if(mUnifiedInterstitialAD!=null&&price>0) {
            mUnifiedInterstitialAD.sendWinNotification(price);
        }
        ADUtil.logError("=======显示优量汇插屏=======");
        /**
         * 先切子线程，再在子线程中切主线程进行广告展示
         */
        ThreadUtils.runOnUIThreadByThreadPool(new Runnable() {
            @Override
            public void run() {

                if (mUnifiedInterstitialAD != null) {
                    if (isServerBidding()) {
                        mUnifiedInterstitialAD.setBidECPM(mUnifiedInterstitialAD.getECPM());
                    }
                    mUnifiedInterstitialAD.show(activity);
                }
            }
        });

    }


    @Override
    public void load(final Context context, AdSlot adSlot, final MediationCustomServiceConfig mediationCustomServiceConfig) {
/**
 * 在子线程中进行广告加载
 */
        ThreadUtils.runOnThreadPool(new Runnable() {
            @Override
            public void run() {
                if (context instanceof Activity) {
                    UnifiedInterstitialADListener unifiedInterstitialADListener = new UnifiedInterstitialADListener() {
                        @Override
                        public void onADReceive() {
                            ADUtil.logError( "onADReceive");
                            if (isClientBidding()) { //bidding类型广告
                                double ecpm = mUnifiedInterstitialAD.getECPM();//当无权限调用该接口时，SDK会返回错误码-1
                                if (ecpm < 0) {
                                    ecpm = 0;
                                }
                                price = (int) ecpm;
                                ADManager.getInstance().setInterstitialYlhErrorInfo(new YlhErrorInfo(BiddingLossReason.LOW_PRICE));
                                ADUtil.logError( "优量汇插屏:" + ecpm);
                                callLoadSuccess(ecpm);
                            } else {
                                callLoadSuccess();
                            }
                        }

                        @Override
                        public void onVideoCached() {
                            ADUtil.logError( "优量汇插屏 onVideoCached");
                        }

                        @Override
                        public void onNoAD(AdError adError) {
                            ADManager.getInstance().setInterstitialYlhErrorInfo(new YlhErrorInfo(BiddingLossReason.NO_AD));
                            if (adError != null) {
                                ADUtil.logError("优量汇插屏 onNoAD errorCode = " + adError.getErrorCode() + " errorMessage = " + adError.getErrorMsg());
                                callLoadFail(adError.getErrorCode(), adError.getErrorMsg());
                            } else {
                                ADUtil.logError("优量汇插屏 单纯error");
                                callLoadFail(ADConst.LOAD_ERROR, "no ad");
                            }
                        }

                        @Override
                        public void onADOpened() {
                            ADUtil.logError("优量汇插屏 onADOpened");
                            callInterstitialAdOpened();
                        }

                        @Override
                        public void onADExposure() {
                            ADUtil.logError("优量汇插屏 onADExposure");
                            callInterstitialShow();
                        }

                        @Override
                        public void onADClicked() {
                            ADUtil.logError("优量汇插屏 onADClicked");
                            callInterstitialAdClick();
                        }

                        @Override
                        public void onADLeftApplication() {
                            ADUtil.logError("优量汇插屏 onADLeftApplication");
                            callInterstitialAdLeftApplication();
                        }

                        @Override
                        public void onADClosed() {
                            ADUtil.logError("优量汇插屏 onADClosed");
                            callInterstitialClosed();
                        }

                        @Override
                        public void onRenderSuccess() {
                            ADUtil.logError("优量汇插屏 onRenderSuccess");
                        }

                        @Override
                        public void onRenderFail() {
                            ADUtil.logError("优量汇插屏 onRenderFail");

                        }
                    };
                    if (isServerBidding()) {
                        mUnifiedInterstitialAD = new UnifiedInterstitialAD((Activity) context, mediationCustomServiceConfig.getADNNetworkSlotId(), unifiedInterstitialADListener, null, getAdm());
                    } else {
                        mUnifiedInterstitialAD = new UnifiedInterstitialAD((Activity) context, mediationCustomServiceConfig.getADNNetworkSlotId(), unifiedInterstitialADListener);
                    }
                    ADManager.getInstance().setInterstitialYlhErrorInfo(new YlhErrorInfo(BiddingLossReason.OTHER));
                    ADManager.getInstance().setInterstitialYlhAD(mUnifiedInterstitialAD);
                    mUnifiedInterstitialAD.loadAD();
                } else {
                    callLoadFail(ADConst.LOAD_ERROR, "context is not Activity");
                }

            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * 在子线程中进行广告销毁
         */
//        ThreadUtils.runOnThreadPool(new Runnable() {
//            @Override
//            public void run() {
        try {
            if (mUnifiedInterstitialAD != null) {
                mUnifiedInterstitialAD.destroy();
                mUnifiedInterstitialAD = null;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
//            }
//        });
    }

    @Override
    public MediationConstant.AdIsReadyStatus isReadyCondition() {
        /**
         * 在子线程中进行广告是否可用的判断
         */
        Future<MediationConstant.AdIsReadyStatus> future = ThreadUtils.runOnThreadPool(new Callable<MediationConstant.AdIsReadyStatus>() {
            @Override
            public MediationConstant.AdIsReadyStatus call() throws Exception {
                if (mUnifiedInterstitialAD != null && mUnifiedInterstitialAD.isValid()) {
                    return MediationConstant.AdIsReadyStatus.AD_IS_READY;
                } else {
                    return MediationConstant.AdIsReadyStatus.AD_IS_NOT_READY;
                }
            }
        });
        try {
            MediationConstant.AdIsReadyStatus result = future.get(500, TimeUnit.MILLISECONDS);//设置500毫秒的总超时，避免线程阻塞
            if (result != null) {
                return result;
            } else {
                return MediationConstant.AdIsReadyStatus.AD_IS_NOT_READY;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return MediationConstant.AdIsReadyStatus.AD_IS_NOT_READY;
    }

    /**
     * 是否clientBidding广告
     *
     * @return
     */
    public boolean isClientBidding() {
        return getBiddingType() == MediationConstant.AD_TYPE_CLIENT_BIDING;
    }

    /**
     * 是否serverBidding广告
     *
     * @return
     */
    public boolean isServerBidding() {
        return getBiddingType() == MediationConstant.AD_TYPE_SERVER_BIDING;
    }
}
