package com.cn.shuangzi.ad.manager;

import android.widget.FrameLayout;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.CSJSplashAd;
import com.bytedance.sdk.openadsdk.TTAdInteractionListener;
import com.bytedance.sdk.openadsdk.TTAdNative;
import com.bytedance.sdk.openadsdk.mediation.ad.MediationAdSlot;
import com.bytedance.sdk.openadsdk.mediation.manager.MediationAdEcpmInfo;
import com.cn.shuangzi.ad.ADManager;
import com.cn.shuangzi.ad.activity.GroMoreSplashActivity;
import com.cn.shuangzi.ad.util.ADConst;
import com.cn.shuangzi.ad.util.ADUtil;
import com.cn.shuangzi.ad.util.SplashUtils;
import com.cn.shuangzi.ad.util.tt.TTAdManagerHolder;
import com.cn.shuangzi.ad.util.tt.UIUtils;
import com.cn.shuangzi.util.SZUtil;

public class AdSplashManager {

    private GroMoreSplashActivity mActivity;

    private CSJSplashAd mTTSplashAd;
    private TTAdNative.CSJSplashAdListener mSplashAdListener;

    public AdSplashManager(GroMoreSplashActivity activity, TTAdNative.CSJSplashAdListener splashAdListener) {
        mActivity = activity;
        mSplashAdListener = splashAdListener;
    }

    /**
     * 先走是否初始化成功判断，再加载开屏
     *
     * @param adUnitId 广告位ID
     */
    private void loadAdReal(String adUnitId,int bottomHeight,String bottomCodeId,String scenarioId) {
        SZUtil.logError("=====开始真正加载开屏=====");
        ADManager.getInstance().clearCurrentSplash();
        TTAdNative adNativeLoader = TTAdManagerHolder.getInstance().createAdNative(mActivity);
        AdSlot adslot = new AdSlot.Builder()
                .setCodeId(adUnitId)
                .setImageAcceptedSize(UIUtils.getScreenWidthInPx(mActivity),UIUtils.getScreenHeightInPx(mActivity)-bottomHeight)
                .setMediationAdSlot(
                        new MediationAdSlot.Builder()
                                .setVolume(0)
                                .setMuted(true)
                                .setAllowShowCloseBtn(true)
                                .setScenarioId(scenarioId)
                                .setMediationSplashRequestInfo(SplashUtils.getGMNetworkRequestInfo(bottomCodeId))
                                .build()
                )
                .build();
        adNativeLoader.loadSplashAd(adslot,mSplashAdListener, ADConst.TIME_OUT);
    }
    /**
     * 判断是否初始化成功，再去加载开屏广告
     */
    public void loadAd(final String adUnitId, final int bottomHeight, final String bottomCodeId, final String scenarioId) {
        SZUtil.logError("=====前置加载开屏，需要判断是否已初始化=====");
        if (!TTAdManagerHolder.isInitSuccess()) {
            SZUtil.logError("=====未初始化SDK，开屏等待初始化结果中=====");
            ADManager.getInstance().addOnAdInitDoneListener(new ADManager.OnAdInitDoneListener() {
                @Override
                public void onTTAdInitDone(boolean isSuccess) {
                    SZUtil.logError("开屏判断初始化结果："+isSuccess);
                    ADManager.getInstance().removeOnAdInitDoneListener(this);
                    if (isSuccess) {
                        loadAdReal(adUnitId,bottomHeight,bottomCodeId,scenarioId);
                    } else {
                        loadAd(adUnitId,bottomHeight,bottomCodeId,scenarioId);
                    }
                }
            });
            ADManager.getInstance().initAd(true);
        } else {
            loadAdReal(adUnitId,bottomHeight,bottomCodeId,scenarioId);
        }

    }

    /**
     * 展示开屏广告
     */
    public void showSplashAd(FrameLayout splashContainer,CSJSplashAd ad){
        mTTSplashAd = ad;
        if(mTTSplashAd!=null&&splashContainer!=null) {
            splashContainer.removeAllViews();
            splashContainer.addView(mTTSplashAd.getSplashView());
        }
    }

    public CSJSplashAd getSplashAd() {
        return mTTSplashAd;
    }


    /**
     * 打印其他信息
     */
    public void printInfo() {
        if (mTTSplashAd != null) {
            /**
             * 获取已经加载的clientBidding ，多阶底价广告的相关信息
             */
            /*List<MediationAdEcpmInfo> gmAdEcpmInfos = mTTSplashAd.getMediationManager().getMultiBiddingEcpm();
            if (gmAdEcpmInfos != null) {
                for (MediationAdEcpmInfo info : gmAdEcpmInfos) {
                    ADUtil.logError("多阶+client相关信息 SdkName" + info.getSdkName()
                            + "  CustomSdkName:" + info.getCustomSdkName()
                            + "  SlotId:" + info.getSlotId()
                            + "  request_id:" + info.getRequestId()
                            + "  AdNetworkRitType:" + info.getRitType()
                            + "  ReqBiddingType:" + info.getReqBiddingType()
                            + "  PreEcpm:" + info.getEcpm()
                            + "  LevelTag:" + info.getLevelTag()
                            + "  ErrorMsg:" + info.getErrorMsg())
                    ;
                }
            }

            *//**
             * 获取实时填充/缓存池中价格最优的代码位信息即相关价格信息，每次只有一个信息
             *//*
            MediationAdEcpmInfo bestInfo = mTTSplashAd.getMediationManager().getBestEcpm();
            if (bestInfo != null) {
                ADUtil.logError("***实时填充/缓存池中价格最优的代码位信息*** "
                        + "  CustomSdkName:" + bestInfo.getCustomSdkName()
                        + "  SlotId:" + bestInfo.getSlotId()
                        + "  request_id:" + bestInfo.getRequestId()
                        + "  AdNetworkRitType:" + bestInfo.getRitType()
                        + "  ReqBiddingType:" + bestInfo.getReqBiddingType()
                        + "  PreEcpm:" + bestInfo.getEcpm()
                        + "  LevelTag:" + bestInfo.getLevelTag()
                        + "  ErrorMsg:" + bestInfo.getErrorMsg());
            }

            *//**
             * 获取获取当前缓存池的全部信息
             *//*
            List<MediationAdEcpmInfo> gmCacheInfos = mTTSplashAd.getMediationManager().getCacheList();
            if (gmCacheInfos != null) {
                for (MediationAdEcpmInfo info : gmCacheInfos) {
                    ADUtil.logError("***缓存池的全部信息*** AdNetworkPlatformId"
                            + "  CustomSdkName:" + info.getCustomSdkName()
                            + "  SlotId:" + info.getSlotId()
                            + "  request_id:" + info.getRequestId()
                            + "  AdNetworkRitType:" + info.getRitType()
                            + "  ReqBiddingType:" + info.getReqBiddingType()
                            + "  PreEcpm:" + info.getEcpm()
                            + "  LevelTag:" + info.getLevelTag()
                            + "  ErrorMsg:" + info.getErrorMsg());
                }
            }*/

            /**
             * 获取获展示广告的部信息
             */
            try {
                MediationAdEcpmInfo showGMAdEcpmInfo = mTTSplashAd.getMediationManager().getShowEcpm();

                if (showGMAdEcpmInfo != null) {
                    ADUtil.logError("展示的开屏广告信息： adSdkName: " + showGMAdEcpmInfo.getSdkName() + "   Ecpm: " + showGMAdEcpmInfo.getEcpm());
                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    public void destroy() {
//        mActivity = null;
        try {
            if(mTTSplashAd!=null) {
                mTTSplashAd.getMediationManager().destroy();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        mTTSplashAd = null;
        mSplashAdListener = null;
    }

}
