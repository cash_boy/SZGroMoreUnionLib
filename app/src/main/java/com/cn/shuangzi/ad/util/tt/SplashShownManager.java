package com.cn.shuangzi.ad.util.tt;

public class SplashShownManager {
    private volatile static SplashShownManager mInstance;
    /**
     * 单例获取SplashClickEyeManager对象
     *
     * @return
     */
    public static SplashShownManager getInstance() {
        if (mInstance == null) {
            synchronized (SplashShownManager.class) {
                if (mInstance == null) {
                    mInstance = new SplashShownManager();
                }
            }
        }
        return mInstance;
    }


    private boolean isSplashShown = false;

    public boolean isSplashShown() {
        return isSplashShown;
    }

    public void setSplashShown(boolean splashShown) {
        isSplashShown = splashShown;
    }
}
