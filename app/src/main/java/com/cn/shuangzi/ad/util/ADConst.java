package com.cn.shuangzi.ad.util;


import com.cn.shuangzi.common.SZConst;

/**
 * Created by CN.
 */

public class ADConst extends SZConst {

    public static int YUAN = 100;
    public static int JIAO = 10;

//    public static int LOAD_ERROR = 40000;
    public static int LOAD_ERROR = -2;
    public static int AD_ERROR = 40001;
    public static int VIDEO_ERROR = 40002;
    public static int RENDER_FAIL = 40003;

    public static final String YLH_PLATFORM = "408";
    public static final int TIME_OUT = 5000;
    public static final int LOAD_AD_INDEX = 0;
    public static final String AD_SERVICE_API_REQUEST_EVENT_ID = "ad_service_api_request";
    public static final String AD_READ_LOCAL_EVENT_ID = "ad_read_local";

    public static final String AD_LOAD_POLICY = "ad_load_policy";
    public static final String AD_LOAD_SPLASH_POLICY = "ad_load_splash_policy";
    public static final String AD_LOAD_NATIVE_POLICY = "ad_load_native_policy";
    public static final String AD_LOAD_SELF_BANNER_POLICY = "ad_load_self_banner_policy";
    public static final String AD_LOAD_SELF_FLOAT_POLICY = "ad_load_self_float_policy";
    public static final String AD_LOAD_INTERACTION_POLICY = "ad_load_interaction_policy";
    public static final String AD_LOAD_BANNER_POLICY = "ad_load_banner_policy";

    public static final String ERROR_CODE = "errorCode";
    public static final String AD_PLATFORM_KEY = "ad_platform";
    public static final String AD_PLATFORM_LOCAL = "local";
    public static final String AD_LOCAL_ERROR_EVENT_ID = "ad_service_error_info";
    public static final String AD_THIRD_PLATFORM_ERROR_EVENT_ID = "ad_third_platform_error_info";
    public static final String AD_SPLASH_EXPOSURE_EVENT_ID = "ad_splash_exposure";
    public static final String AD_PLATFORM_TT = "TOUTIAO";
    public static final String AD_PLATFORM_GDT = "GDT";
    public static final String ERROR_MSG = "errorMsg";


    public static final String AD_CLICK_SKIP_EVENT_ID = "click_skip";
    public static final String AD_SPLASH_SHOW_TIME = "show_time";

    public static final String TAG_SPLASH= "splash";
}
