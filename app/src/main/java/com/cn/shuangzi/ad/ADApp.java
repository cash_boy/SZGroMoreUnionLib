package com.cn.shuangzi.ad;

import com.cn.shuangzi.SZApp;

/**
 * Created by CN.
 */
public abstract class ADApp extends SZApp {

    public static ADApp getInstance() {
        return (ADApp) INSTANCE;
    }

    public abstract String getADPlatformAppName();

    public abstract String getTTAppId();

    public String getGDTAppId(){
        return "";
    };

}
