package com.cn.shuangzi.ad;

import android.os.Bundle;

import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.ad.manager.AdInterstitialFullManager;
import com.cn.shuangzi.ad.util.ADUtil;
import com.cn.shuangzi.ad.util.OnAdClosedListener;
import com.cn.shuangzi.ad.util.TTVideoInteractionAdLoader;
import com.cn.shuangzi.ad.util.tt.SplashShownManager;
import com.cn.shuangzi.ad.view.AlertRemoveAdDialog;
import com.cn.shuangzi.util.SZUtil;

/**
 * Created by CN.
 */
public abstract class ADBaseActivity extends SZBaseActivity {

    public boolean isShownInteractionAd = false;
    public boolean isStop = false;

    public boolean showInteractionAdGroMore(String unitId, Class<?> classVip) {
        return showInteractionAdGroMore(unitId, classVip, true, null);
    }

    public boolean showInteractionAdGroMore(String unitId, String scenarioId, Class<?> classVip) {
        return showInteractionAdGroMore(unitId, classVip, true, scenarioId);
    }

    public boolean showInteractionAdGroMore(String unitId, final Class<?> classVip, final boolean isOnceShowVip, String scenarioId) {
        try {
            if (isShowAd()) {
                if (!isStop && !isShownInteractionAd) {
                    if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                        ADUtil.logError("开始显示插屏");
                        isShownInteractionAd = true;
                        new AdInterstitialFullManager(this, isOnceShowVip ? ADUtil.getBuyVipClass(classVip) : classVip)
                                .loadAd(unitId, scenarioId);
                        return true;
                    } else {
                        ADUtil.logError("不显示插屏");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showInteractionAdGroMoreByDayHint(String unitId, final Class<?> classVip,String scenarioId) {
        try {
            if (isShowAd()) {
                if (!isStop && !isShownInteractionAd) {
                    if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                        ADUtil.logError("开始显示插屏");
                        isShownInteractionAd = true;
                        new AdInterstitialFullManager(this,  ADUtil.getBuyVipClassByDays(classVip))
                                .loadAd(unitId, scenarioId);
                        return true;
                    } else {
                        ADUtil.logError("不显示插屏");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showInteractionAdGroMore(String unitId, OnAdClosedListener onAdClosedListener) {
        return showInteractionAdGroMore(unitId, onAdClosedListener, true, null);
    }

    public boolean showInteractionAdGroMore(String unitId, String scenarioId, OnAdClosedListener onAdClosedListener) {
        return showInteractionAdGroMore(unitId, onAdClosedListener, true, scenarioId);
    }

    public boolean showInteractionAdGroMore(String unitId, OnAdClosedListener onAdClosedListener, boolean isOnceShowVip, String scenarioId) {
        try {
            if (isShowAd()) {
                if (!isStop && !isShownInteractionAd) {
                    if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                        ADUtil.logError("开始显示插屏");
                        isShownInteractionAd = true;
                        new AdInterstitialFullManager(this, null, isOnceShowVip && ADUtil.isFirstShowInteractionAd() ? onAdClosedListener : null)
                                .loadAd(unitId, scenarioId);
                        return true;
                    } else {
                        ADUtil.logError("不显示插屏");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean showInteractionAdGroMoreByDayHint(String unitId, OnAdClosedListener onAdClosedListener, String scenarioId) {
        try {
            if (isShowAd()) {
                if (!isStop && !isShownInteractionAd) {
                    if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                        ADUtil.logError("开始显示插屏");
                        isShownInteractionAd = true;
                        new AdInterstitialFullManager(this, null, ADUtil.isDaysAfterShowInteractionAd() ? onAdClosedListener : null)
                                .loadAd(unitId, scenarioId);
                        return true;
                    } else {
                        ADUtil.logError("不显示插屏");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showInteractionAdGroMoreWithoutInnerJudge(String unitId, OnAdClosedListener onAdClosedListener, boolean isOnceShowVip, String scenarioId) {
        try {
            if (isShowAd()) {
                if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                    ADUtil.logError("开始显示插屏");
                    isShownInteractionAd = true;
                    new AdInterstitialFullManager(this, null, isOnceShowVip && ADUtil.isFirstShowInteractionAd() ? onAdClosedListener : null)
                            .loadAd(unitId, scenarioId);
                    return true;
                } else {
                    ADUtil.logError("不显示插屏");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showInteractionAdGroMoreWithoutInnerJudgeByDays(String unitId, OnAdClosedListener onAdClosedListener, String scenarioId) {
        try {
            if (isShowAd()) {
                if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                    ADUtil.logError("开始显示插屏");
                    isShownInteractionAd = true;
                    new AdInterstitialFullManager(this, null,  ADUtil.isDaysAfterShowInteractionAd() ? onAdClosedListener : null)
                            .loadAd(unitId, scenarioId);
                    return true;
                } else {
                    ADUtil.logError("不显示插屏");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showInteractionAdGroMoreWithRemoveAdPop(String unitId, AlertRemoveAdDialog.OnClickRemoveAdListener onClickRemoveAdListener) {
        return showInteractionAdGroMoreWithRemoveAdPop(unitId, null, onClickRemoveAdListener);
    }

    public boolean showInteractionAdGroMoreWithRemoveAdPop(String unitId, String scenarioId, AlertRemoveAdDialog.OnClickRemoveAdListener onClickRemoveAdListener) {
        try {
            if (isShowAd()) {
                if (!isStop && !isShownInteractionAd) {
                    if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                        ADUtil.logError("开始显示插屏");
                        isShownInteractionAd = true;
                        new AdInterstitialFullManager(this, ADUtil.getOnClickRemoveAdListener(onClickRemoveAdListener))
                                .loadAd(unitId, scenarioId);
                        return true;
                    } else {
                        ADUtil.logError("不显示插屏");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean showInteractionAdGroMoreWithRemoveAdPopByDays(String unitId, String scenarioId, AlertRemoveAdDialog.OnClickRemoveAdListener onClickRemoveAdListener) {
        try {
            if (isShowAd()) {
                if (!isStop && !isShownInteractionAd) {
                    if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                        ADUtil.logError("开始显示插屏");
                        isShownInteractionAd = true;
                        new AdInterstitialFullManager(this, ADUtil.getOnClickRemoveAdListenerByDays(onClickRemoveAdListener))
                                .loadAd(unitId, scenarioId);
                        return true;
                    } else {
                        ADUtil.logError("不显示插屏");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showInteractionAdGroMoreWithoutInnerJudgeWithRemoveAdPop(String unitId, String scenarioId, AlertRemoveAdDialog.OnClickRemoveAdListener onClickRemoveAdListener) {
        try {
            if (isShowAd()) {
                if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                    ADUtil.logError("开始显示插屏");
                    new AdInterstitialFullManager(this, ADUtil.getOnClickRemoveAdListener(onClickRemoveAdListener))
                            .loadAd(unitId, scenarioId);
                    return true;
                } else {
                    ADUtil.logError("不显示插屏");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean showInteractionAdGroMoreWithoutInnerJudgeWithRemoveAdPopByDays(String unitId, String scenarioId, AlertRemoveAdDialog.OnClickRemoveAdListener onClickRemoveAdListener) {
        try {
            if (isShowAd()) {
                if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                    ADUtil.logError("开始显示插屏");
                    new AdInterstitialFullManager(this, ADUtil.getOnClickRemoveAdListenerByDays(onClickRemoveAdListener))
                            .loadAd(unitId, scenarioId);
                    return true;
                } else {
                    ADUtil.logError("不显示插屏");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showInteractionAd(String codeId, Class<?> classVip) {
        return showInteractionAd(codeId, classVip, true);
    }

    public boolean showInteractionAd(String codeId, Class<?> classVip, boolean isOnceShowVip) {
        try {
            if (isShowAd()) {
                if (!isStop && !isShownInteractionAd) {
                    if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                        ADUtil.logError("开始显示新插屏");
                        isShownInteractionAd = true;
                        new TTVideoInteractionAdLoader(getActivity(), codeId, isOnceShowVip ? ADUtil.getBuyVipClass(classVip) : classVip).loadAd();
                        return true;
                    } else {
                        ADUtil.logError("不显示新插屏");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showInteractionAdWithoutInnerJudge(String codeId, Class<?> classVip, boolean isOnceShowVip) {
        try {
            if (isShowAd()) {
                if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                    ADUtil.logError("开始显示新插屏");
                    isShownInteractionAd = true;
                    new TTVideoInteractionAdLoader(getActivity(), codeId, isOnceShowVip ? ADUtil.getBuyVipClass(classVip) : classVip).loadAd();
                    return true;
                } else {
                    ADUtil.logError("不显示新插屏");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showInteractionAd(String codeId, OnAdClosedListener onAdClosedListener) {
        return showInteractionAd(codeId, onAdClosedListener, true);
    }

    public boolean showInteractionAd(String codeId, OnAdClosedListener onAdClosedListener, boolean isOnceShowVip) {
        try {
            if (isShowAd()) {
                if (!isStop && !isShownInteractionAd) {
                    if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                        ADUtil.logError("开始显示新插屏");
                        isShownInteractionAd = true;
                        new TTVideoInteractionAdLoader(getActivity(), codeId, isOnceShowVip && ADUtil.isFirstShowInteractionAd() ? onAdClosedListener : null).loadAd();
                        return true;
                    } else {
                        ADUtil.logError("不显示新插屏");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean showInteractionAdWithoutInnerJudge(String codeId, OnAdClosedListener onAdClosedListener, boolean isOnceShowVip) {
        try {
            if (isShowAd()) {
                if (ADManager.getInstance().isShowGroMoreInteractionAd(SplashShownManager.getInstance().isSplashShown())) {
                    ADUtil.logError("开始显示新插屏");
                    isShownInteractionAd = true;
                    new TTVideoInteractionAdLoader(getActivity(), codeId, isOnceShowVip && ADUtil.isFirstShowInteractionAd() ? onAdClosedListener : null).loadAd();
                    return true;
                } else {
                    ADUtil.logError("不显示新插屏");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isStop = true;
    }

    @Override
    protected void onResume() {
//        if (isShowAd()) {
//            SplashMinWindowManager.getInstance().initSplashClickEyeDataInTwoActivity(this);
//        }
        isStop = false;
        super.onResume();
    }

    public void setShownInteractionAd(boolean shownInteractionAd) {
        if (isOpenResetInteractionAdMode()) {
            isShownInteractionAd = shownInteractionAd;
        }
    }

    public boolean isOpenResetInteractionAdMode() {
        return true;
    }

    public abstract boolean isShowAd();
}
