package com.cn.shuangzi.ad.custom;

import android.app.Activity;
import android.content.Context;

import com.bytedance.sdk.openadsdk.AdSlot;
import com.bytedance.sdk.openadsdk.mediation.MediationConstant;
import com.bytedance.sdk.openadsdk.mediation.bridge.custom.interstitial.MediationCustomInterstitialLoader;
import com.bytedance.sdk.openadsdk.mediation.custom.MediationCustomServiceConfig;
import com.cn.shuangzi.ad.ADManager;
import com.cn.shuangzi.ad.bean.KsErrorInfo;
import com.cn.shuangzi.ad.util.ADConst;
import com.cn.shuangzi.ad.util.ADUtil;
import com.cn.shuangzi.ad.util.ThreadUtils;
import com.kwad.sdk.api.KsAdSDK;
import com.kwad.sdk.api.KsInterstitialAd;
import com.kwad.sdk.api.KsLoadManager;
import com.kwad.sdk.api.KsScene;
import com.kwad.sdk.api.KsVideoPlayConfig;
import com.kwad.sdk.api.model.AdExposureFailedReason;
import com.kwad.sdk.api.model.AdExposureFailureCode;
import com.kwad.sdk.api.model.AdnType;

import java.util.List;

import androidx.annotation.Nullable;

public class KsCustomerInterstitial extends MediationCustomInterstitialLoader {
    private KsInterstitialAd mKsInterstitialAd;
    private int price;
    @Override
    public void load(Context context, AdSlot adSlot, final MediationCustomServiceConfig serviceConfig) {

        /**
         * 在子线程中进行广告加载
         */
        ThreadUtils.runOnThreadPool(new Runnable() {
            @Override
            public void run() {
                if (KsAdSDK.getLoadManager() != null) {
                    long posId = 0;
                    try {
                        posId = Long.parseLong(serviceConfig.getADNNetworkSlotId()); //广告位id
                    } catch (Exception e) {
                        callLoadFail(ADConst.AD_ERROR, "代码位ID不合法");
                        return;
                    }
                    KsScene scene = new KsScene.Builder(posId).build();
                    ADUtil.logError("快手插屏 loadInterstitialAd");
                    KsAdSDK.getLoadManager().loadInterstitialAd(scene, new KsLoadManager.InterstitialAdListener() {
                        @Override
                        public void onError(int i, String s) {
                            ADManager.getInstance().setInterstitialKsErrorInfo(new KsErrorInfo(AdExposureFailureCode.OTHER));
                            ADUtil.logError("快手插屏 onError   errCode:" + i + "  errMsg:" + s);
                            callLoadFail(i, s);
                        }

                        @Override
                        public void onRequestResult(int i) {

                        }

                        @Override
                        public void onInterstitialAdLoad(@Nullable List<KsInterstitialAd> list) {
                            if (list != null && list.size() > 0) {
                                mKsInterstitialAd = list.get(0);
                                ADUtil.logError("=====快手插屏已加载=====");
                                ADManager.getInstance().setKsInterstitialAD(mKsInterstitialAd);
                                if (isClientBidding()) {
                                    double ecpm = mKsInterstitialAd.getECPM();
                                    if (ecpm < 0) {
                                        ecpm = 0;
                                    }
                                    price = (int) ecpm;
                                    ADUtil.logError("快手插屏 onInterstitialAdLoad  ecpm:" + ecpm);
                                    ADManager.getInstance().setInterstitialKsErrorInfo(new KsErrorInfo(new AdExposureFailedReason().setAdnType(AdnType.THIRD_PARTY_AD)));
                                    callLoadSuccess(ecpm);
                                } else {
                                    ADUtil.logError("快手插屏 onInterstitialAdLoad ");
                                    callLoadSuccess();
                                }
                                mKsInterstitialAd.setAdInteractionListener(new KsInterstitialAd.AdInteractionListener() {
                                    @Override
                                    public void onAdClicked() {
                                        ADUtil.logError("快手插屏  onAdClicked");
                                        callInterstitialAdClick();
                                    }

                                    @Override
                                    public void onAdShow() {
                                        ADUtil.logError("快手插屏 onAdShow");
                                        callInterstitialShow();
                                    }

                                    @Override
                                    public void onAdClosed() {
                                        ADUtil.logError("快手插屏 onAdClosed");
                                        callInterstitialClosed();

                                    }

                                    @Override
                                    public void onPageDismiss() {
                                        ADUtil.logError("快手插屏 onPageDismiss");
//                                        callInterstitialClosed();
                                    }

                                    @Override
                                    public void onVideoPlayError(int i, int i1) {

                                    }

                                    @Override
                                    public void onVideoPlayEnd() {

                                    }

                                    @Override
                                    public void onVideoPlayStart() {

                                    }

                                    @Override
                                    public void onSkippedAd() {
                                        ADUtil.logError("快手插屏 onSkippedAd");
//                                        callInterstitialClosed();
                                    }
                                });
                            } else {
                                ADManager.getInstance().setInterstitialKsErrorInfo(new KsErrorInfo(AdExposureFailureCode.OTHER));
                                ADUtil.logError("快手插屏 onError   errCode:-2  errMsg:no data");
                                callLoadFail(ADConst.LOAD_ERROR, "no data");
                            }
                        }
                    });
                }

            }
        });

    }

    @Override
    public void showAd(final Activity activity) {
        ADUtil.logError("=========快手 插屏 显示=========");

        if (mKsInterstitialAd!=null&&price > 0) {
            mKsInterstitialAd.setBidEcpm(price,price-ADConst.YUAN);
            ADUtil.logError("=======上报快手插屏数据======="+price);
        }
        /**
         * 先切子线程，再在子线程中切主线程进行广告展示
         */
        ThreadUtils.runOnUIThreadByThreadPool(new Runnable() {
            @Override
            public void run() {
                if (mKsInterstitialAd != null) {
                    KsVideoPlayConfig videoPlayConfig = new KsVideoPlayConfig.Builder()
                            .videoSoundEnable(true)
                            .build();
                    mKsInterstitialAd.showInterstitialAd(activity, videoPlayConfig);
                }
            }
        });
    }


    /**
     * 是否clientBidding广告
     *
     * @return
     */
    public boolean isClientBidding() {
        return getBiddingType() == MediationConstant.AD_TYPE_CLIENT_BIDING;
    }
}
